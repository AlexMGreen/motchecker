# MOT Checker

MOT Checker is an Android application providing vehicle information for vehicles registered in the UK. All data is sourced using the official [DVSA MOT History API](https://www.check-mot.service.gov.uk/mot-history-api). The application is written entirely in Kotlin and features a modern tech stack.

![Search](screenshots/search.png) ![Result](screenshots/result.png) ![Camera](screenshots/camera.png)

## Features

* Vehicle information including make, model, colour, fuel type, and registration date
* Vehicle Mileage information and history
* MOT status and expiry date
* Full vehicle MOT history, test results, reasons for failure, and advisory notices
* Search using your camera
* Save your vehicles

## Tech Stack

* Written entirely in [Kotlin](https://kotlinlang.org/)
* MVVM architecture + Single activity
* Kotlin coroutines for asynchronous operations
* [Dagger](https://github.com/google/dagger) for dependency injection
* [Retrofit](https://square.github.io/retrofit/) for network requests
* [Moshi](https://github.com/square/moshi) for JSON parsing
* [Jetpack](https://developer.android.com/jetpack)
    * [LiveData](https://developer.android.com/topic/libraries/architecture/livedata) for UI updates
    * [ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel) for seperation of concerns, managing UI related data and handling configuration changes
    * [Room Persistence Library](https://developer.android.com/topic/libraries/architecture/room) for data persistence
* [Lottie](http://airbnb.io/lottie) for animations
* [Google Mobile Vision](https://developers.google.com/vision/) for optical character recognition
* Unit tests written with [JUnit](https://junit.org/junit4/) and [MockK](https://github.com/mockk/mockk)
* [Bitrise](https://app.bitrise.io) for Continuous Integration


## Prerequisites
In order to build and run the project an MOT history API key is required from the DVSA. Instructions for applying for an API key can be found [here](https://www.check-mot.service.gov.uk/mot-history-api). Once aquired, the API key should be added to your `gradle.properties` in a property named `MOT_API_KEY`. 

## To Do
* Improvements to error handling
* Reminders for MOT due date
* Search by VIN
* Dark theme


## License

    Copyright 2019 Alexander Green

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
