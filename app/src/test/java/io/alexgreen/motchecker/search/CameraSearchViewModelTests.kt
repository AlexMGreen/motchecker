package io.alexgreen.motchecker.search

import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Test

class CameraSearchViewModelTests {

    @Test
    fun `getMostLikelyRegistration returns null when no detections received`() {
        CameraSearchViewModel().run {
            assertNull(getMostLikelyRegistration())
        }
    }

    @Test
    fun `invalid numberplates are filtered out`() {
        CameraSearchViewModel().run {
            receiveDetection("ABC")
            assertNull(getMostLikelyRegistration())
        }
    }

    @Test
    fun `invalid numberplates are filtered out when several detections received`() {
        CameraSearchViewModel().run {
            receiveDetection("ABC")
            receiveDetection("ABC")
            receiveDetection("AB55 ABC")
            assertEquals("AB55 ABC", getMostLikelyRegistration())
        }
    }

    @Test
    fun `getMostLikelyRegistration returns expected when one detection received`() {
        CameraSearchViewModel().run {
            receiveDetection("AB55 ABC")
            assertEquals("AB55 ABC", getMostLikelyRegistration())
        }
    }

    @Test
    fun `getMostLikelyRegistration returns expected when multi-line numberplate detection received`() {
        CameraSearchViewModel().run {
            receiveDetection("AB55\nABC")
            assertEquals("AB55 ABC", getMostLikelyRegistration())
        }
    }

    @Test
    fun `getMostLikelyRegistration returns expected when one detection received several times`() {
        CameraSearchViewModel().run {
            receiveDetection("AB55 ABC")
            receiveDetection("AB55 ABC")
            receiveDetection("AB55 ABC")
            assertEquals("AB55 ABC", getMostLikelyRegistration())
        }
    }

    @Test
    fun `getMostLikelyRegistration returns expected when several detections received`() {
        CameraSearchViewModel().run {
            receiveDetection("AB55 ABC")
            receiveDetection("AB55 ABC")
            receiveDetection("CD09 DEF")
            assertEquals("AB55 ABC", getMostLikelyRegistration())
        }
    }

    @Test
    fun `getMostLikelyRegistration returns expected when several detections received but most frequent has been denied`() {
        CameraSearchViewModel().run {
            receiveDetection("AB55 ABC")
            receiveDetection("AB55 ABC")
            receiveDetection("CD09 DEF")
            denySuggestion("AB55 ABC")
            assertEquals("CD09 DEF", getMostLikelyRegistration())
        }
    }
}
