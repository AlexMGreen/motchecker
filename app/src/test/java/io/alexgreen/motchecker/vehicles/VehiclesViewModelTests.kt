package io.alexgreen.motchecker.vehicles

import androidx.lifecycle.LiveData
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import io.mockk.verify
import org.junit.Assert
import org.junit.Test

class VehiclesViewModelTests {
    private val vehicle1 = Vehicle("ABC1", "SomeMake", "SomeModel", "2001.12.10", "Petrol", "Blue")
    private val vehicle2 = Vehicle("DEF", "OtherMake", "OtherModel", "2016.04.04", "Diesel", "Red")
    private val vehicle3 = Vehicle("GHI", "AnotherMake", "AnotherModel", "2012.04.04", "Petrol", "Yellow")
    private val vehicle4 = Vehicle("JKL", "AndAnotherMake", "AndAnotherModel", "1995.04.04", "Diesel", "Black")

    @Test
    fun `allManuallySavedVehicles requests all manually vehicles from repository`() {
        val mockAllVehicles = mockk<LiveData<List<Vehicle>>> {
            every { value } returns listOf()
        }
        val mockRepository = mockk<VehicleRepository> {
            every { getAllManuallySavedVehicles() } returns mockAllVehicles
            every { getRecentVehicles(any()) } returns mockk()
        }
        VehiclesViewModel(mockRepository).run {
            allManuallySavedVehicles.value
            verify(exactly = 1) { mockRepository.getAllManuallySavedVehicles() }
        }
    }

    @Test
    fun `allRecentVehicles requests all recent vehicles from repository`() {
        val mockRepository = mockk<VehicleRepository> {
            every { getAllManuallySavedVehicles() } returns mockk()
            every { getRecentVehicles(any()) } returns mockk()
            every { getAllRecentVehicles() } returns mockk { every { value } returns emptyList() }
        }
        VehiclesViewModel(mockRepository).run {
            allRecentVehicles.value
            verify(exactly = 1) { mockRepository.getAllRecentVehicles() }
        }
    }

    @Test
    fun `allManuallySavedVehicles contains expected number of vehicles`() {
        val mockAllVehicles = mockk<LiveData<List<Vehicle>>> {
            every { value } returns listOf(vehicle1, vehicle2)
        }
        val mockRepository = mockk<VehicleRepository> {
            every { getAllManuallySavedVehicles() } returns mockAllVehicles
            every { getRecentVehicles(any()) } returns mockk()
        }
        VehiclesViewModel(mockRepository).run {
            Assert.assertEquals(2, allManuallySavedVehicles.value?.size)
        }
    }

    @Test
    fun `allManuallySavedVehicles contains expected vehicles`() {
        val mockAllVehicles = mockk<LiveData<List<Vehicle>>> {
            every { value } returns listOf(vehicle1, vehicle2)
        }
        val mockRepository = mockk<VehicleRepository> {
            every { getAllManuallySavedVehicles() } returns mockAllVehicles
            every { getRecentVehicles(any()) } returns mockk()
        }
        VehiclesViewModel(mockRepository).run {
            Assert.assertTrue(allManuallySavedVehicles.value?.contains(vehicle1) == true)
            Assert.assertTrue(allManuallySavedVehicles.value?.contains(vehicle2) == true)
        }
    }

    @Test
    fun `recentVehicles requests all recentVehicles vehicles from repository`() {
        val mockAllVehicles = mockk<LiveData<List<Vehicle>>> {
            every { value } returns listOf()
        }
        val mockRepository = mockk<VehicleRepository> {
            every { getAllManuallySavedVehicles() } returns mockAllVehicles
            every { getRecentVehicles(any()) } returns mockk { every { value } returns emptyList() }
        }
        VehiclesViewModel(mockRepository).run {
            recentVehicles.value
            verify(exactly = 1) { mockRepository.getRecentVehicles(3) }
        }
    }

    @Test
    fun `updateVehicleList marks vehicles as unsaved which are not present in new list`() {
        val mockAllVehicles = mockk<LiveData<List<Vehicle>>> {
            every { value } returns listOf(vehicle1, vehicle2)
        }
        val mockRepository = mockk<VehicleRepository> {
            every { getAllManuallySavedVehicles() } returns mockAllVehicles
            every { getRecentVehicles(any()) } returns mockk()
            coEvery { markVehicleAsUnsaved(any()) } just Runs
            coEvery { updateVehicle(any()) } just Runs
        }
        VehiclesViewModel(mockRepository).run {
            updateVehicleList(listOf(vehicle3, vehicle4))
            coVerify(exactly = 1) { mockRepository.markVehicleAsUnsaved(vehicle1) }
            coVerify(exactly = 1) { mockRepository.markVehicleAsUnsaved(vehicle2) }
        }
    }

    @Test
    fun `updateVehicleList doesn't unsave vehicles present in both lists`() {
        val mockAllVehicles = mockk<LiveData<List<Vehicle>>> {
            every { value } returns listOf(vehicle1, vehicle2)
        }
        val mockRepository = mockk<VehicleRepository> {
            every { getAllManuallySavedVehicles() } returns mockAllVehicles
            every { getRecentVehicles(any()) } returns mockk()
            coEvery { markVehicleAsUnsaved(any()) } just Runs
            coEvery { updateVehicle(any()) } just Runs
        }
        VehiclesViewModel(mockRepository).run {
            updateVehicleList(listOf(vehicle1, vehicle2))
            coVerify(exactly = 0) { mockRepository.markVehicleAsUnsaved(vehicle1) }
            coVerify(exactly = 0) { mockRepository.markVehicleAsUnsaved(vehicle2) }
        }
    }
}
