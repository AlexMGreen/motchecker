package io.alexgreen.motchecker.vehicles

import io.alexgreen.motchecker.db.VehicleDao
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert
import org.junit.Test

class VehicleRepositoryTests {

    @Test
    fun `isVehicleManuallySaved false when null record returned from DAO`() {
        val registration = "ABC123"
        val mockDao = mockk<VehicleDao> {
            coEvery { getVehicle(registration) } returns null
        }
        VehicleRepository(mockDao).run {
            runBlocking {
                Assert.assertFalse(isVehicleManuallySaved(registration))
            }
        }
    }

    @Test
    fun `isVehicleManuallySaved false when non-null record returned from DAO for vehicle not manually saved`() {
        val registration = "ABC123"
        val mockDao = mockk<VehicleDao> {
            coEvery { getVehicle(registration) } returns mockk { every { userSavedManually } returns false }
        }
        VehicleRepository(mockDao).run {
            runBlocking {
                Assert.assertFalse(isVehicleManuallySaved(registration))
            }
        }
    }

    @Test
    fun `isVehicleManuallySaved true when non-null record returned from DAO for vehicle manually saved`() {
        val registration = "ABC123"
        val mockDao = mockk<VehicleDao> {
            coEvery { getVehicle(registration) } returns mockk { every { userSavedManually } returns true }
        }
        VehicleRepository(mockDao).run {
            runBlocking {
                Assert.assertTrue(isVehicleManuallySaved(registration))
            }
        }
    }

    @Test
    fun `saveVehicle calls insertVehicle on DAO`() {
        val vehicle = Vehicle("", "", "", "", "", "")

        val mockDao = mockk<VehicleDao> {
            coEvery { insertVehicle(any()) } just Runs
        }
        VehicleRepository(mockDao).run {
            runBlocking {
                saveVehicle(vehicle)
                coVerify { mockDao.insertVehicle(vehicle) }
            }
        }
    }

    @Test
    fun `getVehicle calls getVehicle on DAO`() {
        val mockDao = mockk<VehicleDao> {
            coEvery { getVehicle(any()) } returns mockk()
        }
        VehicleRepository(mockDao).run {
            runBlocking {
                getVehicle("ABC")
                coVerify { mockDao.getVehicle("ABC") }
            }
        }
    }

    @Test
    fun `getAllManuallySavedVehicles calls getAllManuallySavedVehicles on DAO`() {
        val mockDao = mockk<VehicleDao> {
            coEvery { getAllManuallySavedVehicles() } returns mockk()
        }
        VehicleRepository(mockDao).run {
            runBlocking {
                getAllManuallySavedVehicles()
                coVerify { mockDao.getAllManuallySavedVehicles() }
            }
        }
    }

    @Test
    fun `getRecentVehicles calls getRecentVehicles on DAO`() {
        val mockDao = mockk<VehicleDao> {
            coEvery { getRecentVehicles(any()) } returns mockk()
        }
        VehicleRepository(mockDao).run {
            runBlocking {
                getRecentVehicles(3)
                coVerify { mockDao.getRecentVehicles(3) }
            }
        }
    }

    @Test
    fun `getAllRecentVehicles calls getAllRecentVehicles on DAO`() {
        val mockDao = mockk<VehicleDao> {
            coEvery { getAllRecentVehicles() } returns mockk()
        }
        VehicleRepository(mockDao).run {
            runBlocking {
                getAllRecentVehicles()
                coVerify { mockDao.getAllRecentVehicles() }
            }
        }
    }
}
