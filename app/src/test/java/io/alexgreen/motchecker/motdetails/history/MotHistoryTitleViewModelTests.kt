package io.alexgreen.motchecker.motdetails.history

import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.mot.MotHistory
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert
import org.junit.Test

class MotHistoryTitleViewModelTests {

    @Test
    fun `test title is correct`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns emptyList()
        }
        MotHistoryTitleViewModel(mockMotHistory).run {
            Assert.assertEquals(R.string.mot_history, title)
        }
    }

    @Test
    fun `numberOfTestsPassed correct for empty list of previous mot tests`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns emptyList()
        }
        MotHistoryTitleViewModel(mockMotHistory).run {
            Assert.assertEquals(0, numberOfTestsPassed)
        }
    }

    @Test
    fun `numberOfTestsFailed correct for empty list of previous mot tests`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns emptyList()
        }
        MotHistoryTitleViewModel(mockMotHistory).run {
            Assert.assertEquals(0, numberOfTestsFailed)
        }
    }

    @Test
    fun `numberOfTestsPassed correct for non-empty list of previous mot tests`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { testResult } returns "PASSED" },
                mockk { every { testResult } returns "PASSED" },
                mockk { every { testResult } returns "PASSED" },
                mockk { every { testResult } returns "FAILED" },
                mockk { every { testResult } returns "PASSED" },
                mockk { every { testResult } returns "FAILED" }
            )
        }
        MotHistoryTitleViewModel(mockMotHistory).run {
            Assert.assertEquals(4, numberOfTestsPassed)
        }
    }

    @Test
    fun `numberOfTestsFailed correct for non-empty list of previous mot tests`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { testResult } returns "FAILED" },
                mockk { every { testResult } returns "PASSED" },
                mockk { every { testResult } returns "PASSED" },
                mockk { every { testResult } returns "FAILED" },
                mockk { every { testResult } returns "FAILED" },
                mockk { every { testResult } returns "PASSED" },
                mockk { every { testResult } returns "FAILED" },
                mockk { every { testResult } returns "FAILED" }
            )
        }
        MotHistoryTitleViewModel(mockMotHistory).run {
            Assert.assertEquals(5, numberOfTestsFailed)
        }
    }
}
