package io.alexgreen.motchecker.motdetails.vehicledetails

import io.alexgreen.motchecker.mot.MotHistory
import org.junit.Assert
import org.junit.Test

class VehicleDetailsViewModelTests {
    val motHistory = MotHistory(
        "registration",
        "make",
        "model",
        "2019.05.02",
        "fuelType",
        "primaryColour",
        listOf()
    )

    @Test
    fun `make correctly returned from MotHistory`() {
        VehicleDetailsViewModel(motHistory).run {
            Assert.assertEquals("make", make)
        }
    }

    @Test
    fun `model correctly returned capitalized from MotHistory`() {
        VehicleDetailsViewModel(motHistory).run {
            Assert.assertEquals("Model", model)
        }
    }

    @Test
    fun `model correctly returned capitalized from MotHistory with multiple words`() {
        VehicleDetailsViewModel(motHistory.copy(model = "some model A")).run {
            Assert.assertEquals("Some Model A", model)
        }
    }

    @Test
    fun `color correctly returned from MotHistory`() {
        VehicleDetailsViewModel(motHistory).run {
            Assert.assertEquals("primaryColour", primaryColour)
        }
    }

    @Test
    fun `fuelType correctly returned from MotHistory`() {
        VehicleDetailsViewModel(motHistory).run {
            Assert.assertEquals("fuelType", fuelType)
        }
    }

    @Test
    fun `firstUsedDate correctly returned from MotHistory`() {
        VehicleDetailsViewModel(motHistory).run {
            Assert.assertEquals("02 May 2019", firstUsedDate)
        }
    }
}
