package io.alexgreen.motchecker.motdetails.mileage

import io.alexgreen.motchecker.mot.MotHistory
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert
import org.junit.Test

class MileageTitleViewModelTests {

    @Test
    fun `totalMiles is correct when only 1 previous mot test`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { odometerValue } returns "12345" }
            )
        }
        MileageTitleViewModel(mockMotHistory).run {
            Assert.assertEquals("12,345", totalMiles)
        }
    }

    @Test
    fun `totalMiles is correct when multiple previous mot tests`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { odometerValue } returns "0" },
                mockk { every { odometerValue } returns "956" },
                mockk { every { odometerValue } returns "4764" },
                mockk { every { odometerValue } returns "74706" },
                mockk { every { odometerValue } returns "121654" },
                mockk { every { odometerValue } returns "1012052" },
                mockk { every { odometerValue } returns "1012053" }
            )
        }
        MileageTitleViewModel(mockMotHistory).run {
            Assert.assertEquals("1,012,053", totalMiles)
        }
    }

    @Test
    fun `totalMiles is correct when same mileage on multiple mot tests`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { odometerValue } returns "0" },
                mockk { every { odometerValue } returns "956" },
                mockk { every { odometerValue } returns "4764" },
                mockk { every { odometerValue } returns "74706" },
                mockk { every { odometerValue } returns "74706" }
            )
        }
        MileageTitleViewModel(mockMotHistory).run {
            Assert.assertEquals("74,706", totalMiles)
        }
    }

    @Test
    fun `totalMiles is correctly formatted for 1 digit mileage`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { odometerValue } returns "5" }
            )
        }
        MileageTitleViewModel(mockMotHistory).run {
            Assert.assertEquals("5", totalMiles)
        }
    }

    @Test
    fun `totalMiles is correctly formatted for 2 digit mileage`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { odometerValue } returns "99" }
            )
        }
        MileageTitleViewModel(mockMotHistory).run {
            Assert.assertEquals("99", totalMiles)
        }
    }

    @Test
    fun `totalMiles is correctly formatted for 3 digit mileage`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { odometerValue } returns "471" }
            )
        }
        MileageTitleViewModel(mockMotHistory).run {
            Assert.assertEquals("471", totalMiles)
        }
    }

    @Test
    fun `totalMiles is correctly formatted for 4 digit mileage`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { odometerValue } returns "7565" }
            )
        }
        MileageTitleViewModel(mockMotHistory).run {
            Assert.assertEquals("7,565", totalMiles)
        }
    }

    @Test
    fun `totalMiles is correctly formatted for 5 digit mileage`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { odometerValue } returns "55123" }
            )
        }
        MileageTitleViewModel(mockMotHistory).run {
            Assert.assertEquals("55,123", totalMiles)
        }
    }

    @Test
    fun `totalMiles is correctly formatted for 6 digit mileage`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { odometerValue } returns "666666" }
            )
        }
        MileageTitleViewModel(mockMotHistory).run {
            Assert.assertEquals("666,666", totalMiles)
        }
    }

    @Test
    fun `totalMiles is correctly formatted for 7 digit mileage`() {
        val mockMotHistory = mockk<MotHistory> {
            every { motTests } returns listOf(
                mockk { every { odometerValue } returns "1234567" }
            )
        }
        MileageTitleViewModel(mockMotHistory).run {
            Assert.assertEquals("1,234,567", totalMiles)
        }
    }
}
