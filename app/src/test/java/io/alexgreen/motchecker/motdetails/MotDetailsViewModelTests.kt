package io.alexgreen.motchecker.motdetails

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import io.alexgreen.motchecker.CoroutinesMainDispatcherRule
import io.alexgreen.motchecker.mot.MotHistory
import io.alexgreen.motchecker.mot.MotRepository
import io.alexgreen.motchecker.mot.MotTest
import io.alexgreen.motchecker.mot.Result
import io.alexgreen.motchecker.motdetails.history.MotHistoryTitleViewModel
import io.alexgreen.motchecker.motdetails.history.MotTestViewModel
import io.alexgreen.motchecker.motdetails.mileage.MileageTitleViewModel
import io.alexgreen.motchecker.motdetails.mileage.MileageViewModel
import io.alexgreen.motchecker.motdetails.status.MotStatusViewModel
import io.alexgreen.motchecker.motdetails.vehicledetails.VehicleDetailsViewModel
import io.alexgreen.motchecker.vehicles.VehicleRepository
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.just
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.threeten.bp.Clock
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneOffset

class MotDetailsViewModelTests {
    @ExperimentalCoroutinesApi
    @get:Rule
    var coroutinesMainDispatcherRule = CoroutinesMainDispatcherRule()
    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    private val offset = ZoneOffset.UTC
    private val date = LocalDate.of(2019, 5, 2).atStartOfDay().toInstant(offset)
    private val clock = Clock.fixed(date, offset)
    private val motTest = MotTest(
        "2018.05.03 12:00:00",
        "PASSED",
        "2019.05.02",
        "123456789",
        "mi",
        "12345",
        emptyList()
    )

    @Test
    fun `mot history requested from repository when init called`() {
        val registrationNumber = "S0M3R3G"
        val mockMotRepository = mockk<MotRepository> {
            coEvery { getMotHistory(any()) } returns mockk()
        }
        val mockVehicleRepository = mockk<VehicleRepository> {
            coEvery { saveVehicle(any()) } just Runs
            coEvery { isVehicleManuallySaved(any()) } returns false
        }
        MotDetailsViewModel(clock, mockMotRepository, mockVehicleRepository).run {
            init(registrationNumber)
            motHistory.observeForever { }
            coVerify(exactly = 1) { mockMotRepository.getMotHistory(registrationNumber) }
        }
    }

    @Test
    fun `mot history initially emits Loading state when getMotHistory called`() {
        val registrationNumber = "S0M3R3G"
        val mockMotRepository = mockk<MotRepository> {
            coEvery { getMotHistory(registrationNumber) } returns mockk()
        }
        val mockVehicleRepository = mockk<VehicleRepository> {
            coEvery { saveVehicle(any()) } just Runs
            coEvery { isVehicleManuallySaved(any()) } returns false
        }
        MotDetailsViewModel(clock, mockMotRepository, mockVehicleRepository).run {
            var numberOfResults = 0
            motHistory.observeForever { result ->
                numberOfResults++
                // Only first result should be Result.Loading
                if (numberOfResults == 1) {
                    Assert.assertTrue(result is Result.Loading)
                }
            }
        }
    }

    @Test
    fun `mot history returned from repository when getMotHistory called`() {
        val registrationNumber = "S0M3R3G"
        val mockResult = mockk<Result<MotHistory>>()
        val mockMotRepository = mockk<MotRepository> {
            coEvery { getMotHistory(registrationNumber) } returns mockResult
        }
        val mockVehicleRepository = mockk<VehicleRepository> {
            coEvery { saveVehicle(any()) } just Runs
            coEvery { isVehicleManuallySaved(any()) } returns false
        }
        MotDetailsViewModel(clock, mockMotRepository, mockVehicleRepository).run {
            var numberOfResults = 0
            motHistory.observeForever { result ->
                numberOfResults++
                // First result should be Result.Loading
                if (numberOfResults == 2) {
                    Assert.assertEquals(mockResult, result)
                }
            }
        }
    }

    @Test
    fun `getMotDetailsItems returns empty list for null MotHistory`() {
        val registrationNumber = "S0M3R3G"
        val mockResult = mockk<Result<MotHistory>>()
        val mockMotRepository = mockk<MotRepository> {
            coEvery { getMotHistory(registrationNumber) } returns mockResult
        }
        val mockVehicleRepository = mockk<VehicleRepository> {
            coEvery { saveVehicle(any()) } just Runs
            coEvery { isVehicleManuallySaved(any()) } returns false
        }
        MotDetailsViewModel(clock, mockMotRepository, mockVehicleRepository).run {
            Assert.assertTrue(getMotDetailsItems(null).isEmpty())
        }
    }

    @Test
    fun `getMotDetailsItems returns expected list for non-null MotHistory`() {
        val registrationNumber = "S0M3R3G"
        val mockResult = mockk<Result<MotHistory>>()
        val mockMotRepository = mockk<MotRepository> {
            coEvery { getMotHistory(registrationNumber) } returns mockResult
        }
        val motHistory = mockk<MotHistory>(relaxed = true) {
            every { motTests } returns listOf(motTest)
        }
        val mockVehicleRepository = mockk<VehicleRepository> {
            coEvery { saveVehicle(any()) } just Runs
            coEvery { isVehicleManuallySaved(any()) } returns false
        }
        MotDetailsViewModel(clock, mockMotRepository, mockVehicleRepository).run {
            val items = getMotDetailsItems(motHistory)
            Assert.assertTrue(items.filter { it is VehicleDetailsViewModel }.size == 1)
            Assert.assertTrue(items.filter { it is MotStatusViewModel }.size == 1)
            Assert.assertTrue(items.filter { it is MileageTitleViewModel }.size == 1)
            Assert.assertTrue(items.filter { it is MileageViewModel }.size == 1)
            Assert.assertTrue(items.filter { it is MotHistoryTitleViewModel }.size == 1)
            Assert.assertTrue(items.filter { it is MotTestViewModel }.size == 1)
        }
    }

    @Test
    fun `getMotDetailsItems returns correct number of MotTestViewModels`() {
        val registrationNumber = "S0M3R3G"
        val mockResult = mockk<Result<MotHistory>>()
        val mockMotRepository = mockk<MotRepository> {
            coEvery { getMotHistory(registrationNumber) } returns mockResult
        }
        val motHistory = mockk<MotHistory>(relaxed = true) {
            every { motTests } returns listOf(motTest, motTest.copy(), motTest.copy())
        }
        val mockVehicleRepository = mockk<VehicleRepository> {
            coEvery { saveVehicle(any()) } just Runs
            coEvery { isVehicleManuallySaved(any()) } returns false
        }
        MotDetailsViewModel(clock, mockMotRepository, mockVehicleRepository).run {
            val items = getMotDetailsItems(motHistory)
            Assert.assertTrue(items.filter { it is MotTestViewModel }.size == 3)
        }
    }

    @Test
    fun `saveVehicle called on repository when init called`() {
        val registrationNumber = "S0M3R3G"
        val mockMotRepository = mockk<MotRepository> {
            coEvery { getMotHistory(any()) } returns mockk()
        }
        val mockVehicleRepository = mockk<VehicleRepository> {
            coEvery { saveVehicle(any()) } just Runs
            coEvery { isVehicleManuallySaved(any()) } returns false
        }
        MotDetailsViewModel(clock, mockMotRepository, mockVehicleRepository).run {
            init(registrationNumber)
            motHistory.observeForever { result ->
                if (result is Result.Success) {
                    coVerify(exactly = 1) { mockVehicleRepository.saveVehicle(any()) }
                }
            }
        }
    }

    @Test
    fun `isVehicleManuallySaved false when vehicle not saved in repository`() {
        val registrationNumber = "S0M3R3G"
        val mockMotRepository = mockk<MotRepository> {
            coEvery { getMotHistory(any()) } returns mockk()
        }
        val mockVehicleRepository = mockk<VehicleRepository> {
            coEvery { isVehicleManuallySaved(any()) } returns false
        }
        MotDetailsViewModel(clock, mockMotRepository, mockVehicleRepository).run {
            init(registrationNumber)
            motHistory.observeForever { result ->
                if (result is Result.Success) {
                    Assert.assertTrue(isVehicleManuallySaved.value == false)
                }
            }
        }
    }

    @Test
    fun `isVehicleManuallySaved true after updating vehicle`() {
        val registrationNumber = "S0M3R3G"
        val mockMotRepository = mockk<MotRepository> {
            coEvery { getMotHistory(any()) } returns mockk()
        }
        val mockVehicleRepository = mockk<VehicleRepository> {
            coEvery { isVehicleManuallySaved(any()) } returns false
        }
        MotDetailsViewModel(clock, mockMotRepository, mockVehicleRepository).run {
            init(registrationNumber)
            motHistory.observeForever { result ->
                if (result is Result.Success) {
                    Assert.assertTrue(isVehicleManuallySaved.value == false)
                    updateVehicleAsManuallySaved()
                    Assert.assertTrue(isVehicleManuallySaved.value == true)
                }
            }
        }
    }
}
