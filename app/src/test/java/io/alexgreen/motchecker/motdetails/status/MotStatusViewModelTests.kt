package io.alexgreen.motchecker.motdetails.status

import io.alexgreen.motchecker.mot.MotHistory
import io.alexgreen.motchecker.mot.MotTest
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert
import org.junit.Test
import org.threeten.bp.Clock
import org.threeten.bp.LocalDate
import org.threeten.bp.ZoneOffset

class MotStatusViewModelTests {

    @Test
    fun `hasValidMot true when expiry date same as current date`() {
        val offset = ZoneOffset.UTC
        val currentDate = LocalDate.of(2019, 5, 2).atStartOfDay().toInstant(offset)
        val clock = Clock.fixed(currentDate, offset)

        val mockTest = mockk<MotTest> {
            every { expiryDate } returns "2019.05.02"
        }
        val mockHistory = mockk<MotHistory>(relaxed = true) {
            every { motTests } returns listOf(mockTest)
        }

        MotStatusViewModel(mockHistory, clock).run {
            Assert.assertTrue(hasValidMot)
        }
    }

    @Test
    fun `hasValidMot true when expiry date same as current date with several tests`() {
        val offset = ZoneOffset.UTC
        val currentDate = LocalDate.of(2019, 5, 2).atStartOfDay().toInstant(offset)
        val clock = Clock.fixed(currentDate, offset)

        val mockHistory = mockk<MotHistory>(relaxed = true) {
            every { motTests } returns listOf(
                mockk { every { expiryDate } returns "2019.05.02" },
                mockk { every { expiryDate } returns "2018.05.03" },
                mockk { every { expiryDate } returns "2017.04.28" }
            )
        }

        MotStatusViewModel(mockHistory, clock).run {
            Assert.assertTrue(hasValidMot)
        }
    }

    @Test
    fun `hasValidMot true when expiry date after current date`() {
        val offset = ZoneOffset.UTC
        val currentDate = LocalDate.of(2019, 5, 2).atStartOfDay().toInstant(offset)
        val clock = Clock.fixed(currentDate, offset)

        val mockTest = mockk<MotTest> {
            every { expiryDate } returns "2019.05.03"
        }
        val mockHistory = mockk<MotHistory>(relaxed = true) {
            every { motTests } returns listOf(mockTest)
        }

        MotStatusViewModel(mockHistory, clock).run {
            Assert.assertTrue(hasValidMot)
        }
    }

    @Test
    fun `hasValidMot true when expiry date after current date with several tests`() {
        val offset = ZoneOffset.UTC
        val currentDate = LocalDate.of(2019, 5, 2).atStartOfDay().toInstant(offset)
        val clock = Clock.fixed(currentDate, offset)

        val mockHistory = mockk<MotHistory>(relaxed = true) {
            every { motTests } returns listOf(
                mockk { every { expiryDate } returns "2019.05.03" },
                mockk { every { expiryDate } returns "2018.05.03" },
                mockk { every { expiryDate } returns "2017.04.28" }
            )
        }

        MotStatusViewModel(mockHistory, clock).run {
            Assert.assertTrue(hasValidMot)
        }
    }

    @Test
    fun `hasValidMot false when expiry date before current date`() {
        val offset = ZoneOffset.UTC
        val currentDate = LocalDate.of(2019, 5, 2).atStartOfDay().toInstant(offset)
        val clock = Clock.fixed(currentDate, offset)

        val mockTest = mockk<MotTest> {
            every { expiryDate } returns "2019.05.01"
        }
        val mockHistory = mockk<MotHistory>(relaxed = true) {
            every { motTests } returns listOf(mockTest)
        }

        MotStatusViewModel(mockHistory, clock).run {
            Assert.assertFalse(hasValidMot)
        }
    }

    @Test
    fun `hasValidMot false when expiry date before current date with several tests`() {
        val offset = ZoneOffset.UTC
        val currentDate = LocalDate.of(2019, 5, 2).atStartOfDay().toInstant(offset)
        val clock = Clock.fixed(currentDate, offset)

        val mockHistory = mockk<MotHistory>(relaxed = true) {
            every { motTests } returns listOf(
                mockk { every { expiryDate } returns "2019.05.01" },
                mockk { every { expiryDate } returns "2018.05.03" },
                mockk { every { expiryDate } returns "2017.04.28" }
            )
        }

        MotStatusViewModel(mockHistory, clock).run {
            Assert.assertFalse(hasValidMot)
        }
    }

    @Test
    fun `latest expiry date returns expected when only one test`() {
        val offset = ZoneOffset.UTC
        val currentDate = LocalDate.of(2019, 5, 2).atStartOfDay().toInstant(offset)
        val clock = Clock.fixed(currentDate, offset)

        val mockHistory = mockk<MotHistory>(relaxed = true) {
            every { motTests } returns listOf(
                mockk { every { expiryDate } returns "2019.05.01" }
            )
        }

        MotStatusViewModel(mockHistory, clock).run {
            Assert.assertEquals(LocalDate.of(2019, 5, 1), latestExpiryDate)
        }
    }

    @Test
    fun `latest expiry date returns expected when several tests`() {
        val offset = ZoneOffset.UTC
        val currentDate = LocalDate.of(2019, 5, 2).atStartOfDay().toInstant(offset)
        val clock = Clock.fixed(currentDate, offset)

        val mockHistory = mockk<MotHistory>(relaxed = true) {
            every { motTests } returns listOf(
                mockk { every { expiryDate } returns "2019.05.01" },
                mockk { every { expiryDate } returns "2018.05.03" },
                mockk { every { expiryDate } returns "2017.04.28" }
            )
        }

        MotStatusViewModel(mockHistory, clock).run {
            Assert.assertEquals(LocalDate.of(2019, 5, 1), latestExpiryDate)
        }
    }
}
