package io.alexgreen.motchecker.motdetails.mileage

import com.github.mikephil.charting.data.Entry
import io.alexgreen.motchecker.mot.MotHistory
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert
import org.junit.Test

class MileageViewModelTests {

    @Test
    fun `mileageEntries contains correct number of entries for single previous mot test`() {
        val mockMotHistory = mockk<MotHistory> {
            every { firstUsedDate } returns "2002.12.16"
            every { motTests } returns listOf(
                mockk {
                    every { completedDate } returns "2018.05.20 11:28:34"
                    every { odometerValue } returns "12345"
                }
            )
        }
        MileageViewModel(mockMotHistory).run {
            Assert.assertTrue(mileageEntries.size == 2)
        }
    }

    @Test
    fun `mileageEntries contains correct entries for single previous mot test`() {
        val mockMotHistory = mockk<MotHistory> {
            every { firstUsedDate } returns "2002.12.16"
            every { motTests } returns listOf(
                mockk {
                    every { completedDate } returns "2018.05.20 11:28:34"
                    every { odometerValue } returns "12345"
                }
            )
        }
        MileageViewModel(mockMotHistory).run {
            Assert.assertTrue(mileageEntries[0].equalTo(Entry(12037f, 0f)))
            Assert.assertTrue(mileageEntries[1].equalTo(Entry(17671f, 12345f)))
        }
    }

    @Test
    fun `mileageEntries contains correct number of entries for several previous mot tests`() {
        val mockMotHistory = mockk<MotHistory> {
            every { firstUsedDate } returns "2002.12.16"
            every { motTests } returns listOf(
                mockk {
                    every { completedDate } returns "2018.05.20 11:28:34"
                    every { odometerValue } returns "12345"
                },
                mockk {
                    every { completedDate } returns "2017.05.18 14:32:21"
                    every { odometerValue } returns "12345"
                },
                mockk {
                    every { completedDate } returns "2016.05.15 07:56:11"
                    every { odometerValue } returns "12345"
                }
            )
        }
        MileageViewModel(mockMotHistory).run {
            Assert.assertTrue(mileageEntries.size == 4)
        }
    }

    @Test
    fun `mileageEntries contains correct entries for several previous mot tests`() {
        val mockMotHistory = mockk<MotHistory> {
            every { firstUsedDate } returns "2002.12.16"
            every { motTests } returns listOf(
                mockk {
                    every { completedDate } returns "2017.05.18 14:32:21"
                    every { odometerValue } returns "54321"
                },
                mockk {
                    every { completedDate } returns "2018.05.20 11:28:34"
                    every { odometerValue } returns "555555"
                },
                mockk {
                    every { completedDate } returns "2016.05.15 07:56:11"
                    every { odometerValue } returns "12345"
                }
            )
        }
        MileageViewModel(mockMotHistory).run {
            Assert.assertTrue(mileageEntries[0].equalTo(Entry(12037f, 0f)))
            Assert.assertTrue(mileageEntries[1].equalTo(Entry(16936f, 12345f)))
            Assert.assertTrue(mileageEntries[2].equalTo(Entry(17304f, 54321f)))
            Assert.assertTrue(mileageEntries[3].equalTo(Entry(17671f, 555555f)))
        }
    }
}
