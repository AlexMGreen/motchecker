package io.alexgreen.motchecker.motdetails.history

import io.alexgreen.motchecker.mot.MotTest
import io.alexgreen.motchecker.mot.RfrAndComment
import org.junit.Assert
import org.junit.Test
import org.threeten.bp.LocalDate

class MotTestViewModelTests {
    private val motTest = MotTest(
        "2018.05.03 12:00:00",
        "PASSED",
        "2019.05.02",
        "12345",
        "mi",
        "123456789012",
        emptyList()
    )

    @Test
    fun `didPass true when expected`() {
        MotTestViewModel(motTest).run {
            Assert.assertTrue(didPass)
        }
    }

    @Test
    fun `didPass false when expected`() {
        MotTestViewModel(motTest.copy(testResult = "FAILED")).run {
            Assert.assertFalse(didPass)
        }
    }

    @Test
    fun `completedDate as expected`() {
        MotTestViewModel(motTest).run {
            Assert.assertEquals(LocalDate.of(2018, 5, 3), completedDate)
        }
    }

    @Test
    fun `expiryDate as expected`() {
        MotTestViewModel(motTest).run {
            Assert.assertEquals("02 May 2019", expiryDate)
        }
    }

    @Test
    fun `testMileage as expected`() {
        MotTestViewModel(motTest).run {
            Assert.assertEquals("12,345", testMileage)
        }
    }

    @Test
    fun `testNumber as expected`() {
        MotTestViewModel(motTest).run {
            Assert.assertEquals("123456789012", testNumber)
        }
    }

    @Test
    fun `reasonsForFailure empty when expected`() {
        MotTestViewModel(motTest).run {
            Assert.assertTrue(reasonsForFailure.isEmpty())
        }
    }

    @Test
    fun `reasonsForFailure contains expected for RfcAndComment with "DANGEROUS" type`() {
        val dangerousRfcAndComment = RfrAndComment("Some comment", type = "DANGEROUS")
        val motTest = motTest.copy(
            rfrAndComments = listOf(
                dangerousRfcAndComment,
                RfrAndComment("Some advisory", type = "ADVISORY")
            )
        )
        MotTestViewModel(motTest).run {
            Assert.assertEquals(1, reasonsForFailure.size)
            Assert.assertTrue(reasonsForFailure.contains(dangerousRfcAndComment))
        }
    }

    @Test
    fun `reasonsForFailure contains expected for multiple dangerous RfcAndComments`() {
        val dangerousRfcAndComment = RfrAndComment("Some comment", "DANGEROUS")
        val dangerousRfcAndComment2 = RfrAndComment("Some comment2", "DANGEROUS")
        val motTest = motTest.copy(
            rfrAndComments = listOf(
                dangerousRfcAndComment,
                RfrAndComment("Some advisory", type = "ADVISORY"),
                dangerousRfcAndComment2,
                RfrAndComment("Some other advisory", type = "ADVISORY")
            )
        )
        MotTestViewModel(motTest).run {
            Assert.assertEquals(2, reasonsForFailure.size)
            Assert.assertTrue(reasonsForFailure.contains(dangerousRfcAndComment))
            Assert.assertTrue(reasonsForFailure.contains(dangerousRfcAndComment2))
        }
    }

    @Test
    fun `reasonsForFailure contains expected for multiple FAIL RfcAndComments`() {
        val failRfcAndComment = RfrAndComment("Some comment", "FAIL")
        val failRfcAndComment2 = RfrAndComment("Some other comment", "FAIL")
        val motTest = motTest.copy(
            rfrAndComments = listOf(
                RfrAndComment("Some comment", "USER ENTERED"),
                failRfcAndComment,
                RfrAndComment("Some advisory", type = "ADVISORY"),
                RfrAndComment("Some other advisory", type = "ADVISORY"),
                failRfcAndComment2
            )
        )
        MotTestViewModel(motTest).run {
            Assert.assertEquals(2, reasonsForFailure.size)
            Assert.assertTrue(reasonsForFailure.contains(failRfcAndComment))
            Assert.assertTrue(reasonsForFailure.contains(failRfcAndComment2))
        }
    }

    @Test
    fun `reasonsForFailure contains expected for multiple PRS RfcAndComments`() {
        val prsRfcAndComment = RfrAndComment("Some comment", "PRS")
        val prsRfcAndComment2 = RfrAndComment("Some other comment", "PRS")
        val motTest = motTest.copy(
            rfrAndComments = listOf(
                RfrAndComment("Some comment", "USER ENTERED"),
                prsRfcAndComment,
                RfrAndComment("Some advisory", type = "ADVISORY"),
                RfrAndComment("Some other advisory", type = "ADVISORY"),
                prsRfcAndComment2
            )
        )
        MotTestViewModel(motTest).run {
            Assert.assertEquals(2, reasonsForFailure.size)
            Assert.assertTrue(reasonsForFailure.contains(prsRfcAndComment))
            Assert.assertTrue(reasonsForFailure.contains(prsRfcAndComment2))
        }
    }

    @Test
    fun `minorDefects empty when expected`() {
        MotTestViewModel(motTest).run {
            Assert.assertTrue(minorDefects.isEmpty())
        }
    }

    @Test
    fun `minorDefects contains expected for multiple minor RfcAndComments`() {
        val minorRfcAndComment = RfrAndComment("Some comment", "MINOR")
        val minorRfcAndComment2 = RfrAndComment("Some other comment", "MINOR")
        val motTest = motTest.copy(
            rfrAndComments = listOf(
                RfrAndComment("Some comment", "USER ENTERED"),
                RfrAndComment("Some comment", "MAJOR"),
                minorRfcAndComment,
                RfrAndComment("Some advisory", type = "ADVISORY"),
                RfrAndComment("Some other advisory", type = "ADVISORY"),
                minorRfcAndComment2
            )
        )
        MotTestViewModel(motTest).run {
            Assert.assertEquals(2, minorDefects.size)
            Assert.assertTrue(minorDefects.contains(minorRfcAndComment))
            Assert.assertTrue(minorDefects.contains(minorRfcAndComment2))
        }
    }

    @Test
    fun `advisories empty when expected`() {
        MotTestViewModel(motTest).run {
            Assert.assertTrue(advisories.isEmpty())
        }
    }

    @Test
    fun `advisories contains expected for multiple advisories RfcAndComments`() {
        val advisory = RfrAndComment("Some comment", "ADVISORY")
        val advisory2 = RfrAndComment("Some other comment", "ADVISORY")
        val motTest = motTest.copy(
            rfrAndComments = listOf(
                RfrAndComment("Some comment", "USER ENTERED"),
                RfrAndComment("Some comment", "FAIL"),
                advisory,
                RfrAndComment("Some comment", type = "MINOR"),
                RfrAndComment("Some other comment", type = "DANGEROUS"),
                advisory2
            )
        )
        MotTestViewModel(motTest).run {
            Assert.assertEquals(2, advisories.size)
            Assert.assertTrue(advisories.contains(advisory))
            Assert.assertTrue(advisories.contains(advisory2))
        }
    }
}
