package io.alexgreen.motchecker.mot

import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody
import org.junit.Assert
import org.junit.Test
import retrofit2.Response

class MotRepositoryTests {

    @Suppress("DeferredResultUnused")
    @Test
    fun `getMotHistory requests mot history from MotService`() {
        val mockResponse = mockk<Response<List<MotHistory>>> {
            every { isSuccessful } returns true
            every { body() } returns listOf(mockk())
        }
        val mockService = mockk<MotService> {
            every { getMotHistoryAsync(any()) } returns GlobalScope.async { mockResponse }
        }

        MotRepository(mockService).run {
            runBlocking {
                getMotHistory("SOMEREG")
                verify(exactly = 1) { mockService.getMotHistoryAsync(any()) }
            }
        }
    }

    @Suppress("DeferredResultUnused")
    @Test
    fun `getMotHistory requests mot history from MotService with correct registration number`() {
        val mockResponse = mockk<Response<List<MotHistory>>> {
            every { isSuccessful } returns true
            every { body() } returns listOf(mockk())
        }
        val mockService = mockk<MotService> {
            every { getMotHistoryAsync(any()) } returns GlobalScope.async { mockResponse }
        }

        MotRepository(mockService).run {
            val reg = "AB12 CDE"
            runBlocking {
                getMotHistory(reg)
                verify(exactly = 1) { mockService.getMotHistoryAsync(reg) }
            }
        }
    }

    @Test
    fun `getMotHistory returns Error result when api response not successful`() {
        val mockErrorBody = mockk<ResponseBody> {
            every { string() } returns ""
        }
        val mockResponse = mockk<Response<List<MotHistory>>> {
            every { isSuccessful } returns false
            every { errorBody() } returns mockErrorBody
        }
        val mockService = mockk<MotService> {
            every { getMotHistoryAsync(any()) } returns GlobalScope.async { mockResponse }
        }

        MotRepository(mockService).run {
            val reg = "AB12 CDE"
            runBlocking {
                val result = getMotHistory(reg)
                Assert.assertTrue(result is Result.Error)
            }
        }
    }

    @Test
    fun `getMotHistory returned Error contains error body string when api response not successful`() {
        val errorBody = "someErrorBody"
        val mockErrorBody = mockk<ResponseBody> {
            every { string() } returns errorBody
        }
        val mockResponse = mockk<Response<List<MotHistory>>> {
            every { isSuccessful } returns false
            every { errorBody() } returns mockErrorBody
        }
        val mockService = mockk<MotService> {
            every { getMotHistoryAsync(any()) } returns GlobalScope.async { mockResponse }
        }

        MotRepository(mockService).run {
            val reg = "AB12 CDE"
            runBlocking {
                val result = getMotHistory(reg)
                Assert.assertEquals(errorBody, (result as? Result.Error)?.message)
            }
        }
    }

    @Test
    fun `getMotHistory returns Success result when api response is successful`() {
        val mockResponse = mockk<Response<List<MotHistory>>> {
            every { isSuccessful } returns true
            every { body() } returns listOf(mockk())
        }
        val mockService = mockk<MotService> {
            every { getMotHistoryAsync(any()) } returns GlobalScope.async { mockResponse }
        }

        MotRepository(mockService).run {
            val reg = "AB12 CDE"
            runBlocking {
                val result = getMotHistory(reg)
                Assert.assertTrue(result is Result.Success)
            }
        }
    }

    @Test
    fun `getMotHistory returned Success contains response body data when api response is successful`() {
        val mockResponseBody = mockk<MotHistory>()
        val mockResponse = mockk<Response<List<MotHistory>>> {
            every { isSuccessful } returns true
            every { body() } returns listOf(mockResponseBody)
        }
        val mockService = mockk<MotService> {
            every { getMotHistoryAsync(any()) } returns GlobalScope.async { mockResponse }
        }

        MotRepository(mockService).run {
            val reg = "AB12 CDE"
            runBlocking {
                val result = getMotHistory(reg)
                Assert.assertEquals(mockResponseBody, (result as? Result.Success)?.data)
            }
        }
    }
}
