package io.alexgreen.motchecker.mot

/**
 * The response of a network request.
 */
sealed class Result<out T : Any> {

    /**
     * A successful request, containing [data].
     */
    data class Success<out T : Any>(val data: T) : Result<T>()

    /**
     * An unsuccessful request, containing an error [message].
     */
    data class Error(val message: String) : Result<Nothing>()

    /**
     * A request which is currently loading.
     */
    object Loading : Result<Nothing>()
}
