package io.alexgreen.motchecker.mot

import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Defines the MOT History API. Documented at https://www.check-mot.service.gov.uk/mot-history-api.
 */
interface MotService {
    @GET("trade/vehicles/mot-tests")
    fun getMotHistoryAsync(@Query("registration") registrationNumber: String): Deferred<Response<List<MotHistory>>>
}
