package io.alexgreen.motchecker.mot

import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter
import java.text.DecimalFormat

/**
 * Models the history response of an API request for MOT history for a vehicle.
 */
data class MotHistory(
    val registration: String,
    val make: String,
    val model: String,
    val firstUsedDate: String,
    val fuelType: String,
    val primaryColour: String,
    val motTests: List<MotTest>
)

private val dateFormatter = DateTimeFormatter.ofPattern("yyyy.MM.dd")
/**
 * A [LocalDate] object parsed from the [MotHistory.firstUsedDate]'s original "yyyy.MM.dd" format.
 */
val MotHistory.parsedFirstUsedDate: LocalDate
    get() = LocalDate.parse(firstUsedDate, dateFormatter)

/**
 * Models a single MOT test within a [MotHistory].
 */
data class MotTest(
    val completedDate: String,
    val testResult: String,
    val expiryDate: String?,
    val odometerValue: String,
    val odometerUnit: String,
    val motTestNumber: String,
    val rfrAndComments: List<RfrAndComment>
)

/**
 * A [LocalDate] object parsed from the [MotTest.completedDate]'s original "yyyy.MM.dd" format.
 */
val MotTest.parsedCompletedDate: LocalDate
    get() = LocalDate.parse(completedDate, completedDateFormatter)

/**
 * A [LocalDate] object parsed from the [MotTest.expiryDate]'s original "yyyy.MM.dd" format.
 */
val MotTest.parsedExpiryDate: LocalDate?
    get() = expiryDate?.let { expiryDate ->
        LocalDate.parse(expiryDate, dateFormatter)
    }

/**
 * A [String] produced by formatting [MotTest.odometerValue] to a comma-separated number.
 */
val MotTest.formattedMileage: String
    get() = mileageFormatter.format(odometerValue.toInt())

/**
 * Whether or not the [MotTest] resulted in a pass.
 */
val MotTest.didPass: Boolean
    get() = testResult == "PASSED"

private val completedDateFormatter = DateTimeFormatter.ofPattern("yyyy.MM.dd HH:mm:ss")
private val mileageFormatter = DecimalFormat("#,###,###")

/**
 * Models the Rfr and comment with a single [MotTest].
 */
data class RfrAndComment(
    val text: String,
    val type: String
)
