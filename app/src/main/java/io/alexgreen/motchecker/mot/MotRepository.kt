package io.alexgreen.motchecker.mot

import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository to retrieve MOT history information using the [MotService].
 */
@Singleton
class MotRepository @Inject constructor(private val motService: MotService) {

    /**
     * Retrieves MOT history from the [motService] for the given [registrationNumber]. The Response will be checked
     * for success and an appropriate [Result] returned.
     */
    suspend fun getMotHistory(registrationNumber: String): Result<MotHistory> = try {
        val response = motService.getMotHistoryAsync(registrationNumber).await()
        if (response.isSuccessful) {
            Result.Success(response.body()?.first()!!)
        } else {
            Result.Error(response.errorBody()?.string() ?: "")
        }
    } catch (exception: IOException) {
        Result.Error(exception.message ?: "")
    }
}
