package io.alexgreen.motchecker.settings

import androidx.lifecycle.ViewModel

/**
 * ViewModel used to facilitate with configuring application settings.
 */
class SettingsViewModel : ViewModel()
