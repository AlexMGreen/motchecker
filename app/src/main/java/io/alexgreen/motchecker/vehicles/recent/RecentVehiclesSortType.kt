package io.alexgreen.motchecker.vehicles.recent

/**
 * The possible sort types for the recent vehicles list.
 */
enum class RecentVehiclesSortType {
    SearchDate,
    Make,
    Model,
    Registration
}
