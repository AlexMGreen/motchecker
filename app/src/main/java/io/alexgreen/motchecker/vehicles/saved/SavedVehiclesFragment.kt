package io.alexgreen.motchecker.vehicles.saved

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import dagger.android.support.DaggerFragment
import io.alexgreen.motchecker.AppBarHost
import io.alexgreen.motchecker.FabHost
import io.alexgreen.motchecker.NavHost
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.extensions.animateFab
import io.alexgreen.motchecker.extensions.slideIn
import io.alexgreen.motchecker.extensions.slideOut
import io.alexgreen.motchecker.extensions.visibleIf
import io.alexgreen.motchecker.motdetails.MotDetailsFragment
import io.alexgreen.motchecker.vehicles.Vehicle
import io.alexgreen.motchecker.vehicles.VehicleMoveTouchHelperCallback
import io.alexgreen.motchecker.vehicles.VehiclesViewModel
import kotlinx.android.synthetic.main.vehicles_fragment.*
import javax.inject.Inject

/**
 * Fragment used to display to the user a list of vehicles which have previously been saved.
 */
@Suppress("TooManyFunctions")
class SavedVehiclesFragment : DaggerFragment(), VehicleListListener {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(VehiclesViewModel::class.java)
    }
    private val itemTouchHelper: ItemTouchHelper by lazy {
        ItemTouchHelper(VehicleMoveTouchHelperCallback(vehiclesAdapter::onRowMoved))
    }
    private val vehiclesAdapter = SavedVehiclesAdapter(this)
    private lateinit var appBarHost: AppBarHost
    private lateinit var fabHost: FabHost
    private lateinit var navHost: NavHost

    companion object {
        fun newInstance() = SavedVehiclesFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        appBarHost = parentFragment as? AppBarHost
            ?: throw UnsupportedOperationException("Parent fragment must implement AppBarHost")
        fabHost = parentFragment as? FabHost
            ?: throw UnsupportedOperationException("Parent fragment must implement FabHost")
        navHost = parentFragment as? NavHost
            ?: throw UnsupportedOperationException("Parent fragment must implement NavHost")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.vehicles_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupVehiclesList()
        viewModel.allManuallySavedVehicles.observe(viewLifecycleOwner, Observer { vehicles ->
            vehiclesList.visibleIf { vehicles.isNotEmpty() }
            vehiclesEmptyView.visibleIf { vehicles.isEmpty() }
            vehiclesAdapter.submitList(vehicles)
        })
    }

    override fun onResume() {
        super.onResume()
        setupFabs()
    }

    override fun onItemDragged(viewHolder: SavedVehiclesAdapter.SavedVehicleViewHolder) =
        itemTouchHelper.startDrag(viewHolder)

    override fun onItemClicked(vehicle: Vehicle) = requestMotHistory(vehicle.registration)

    private fun setupVehiclesList() {
        with(vehiclesList) {
            itemTouchHelper.attachToRecyclerView(this)
            adapter = vehiclesAdapter
            layoutManager = LinearLayoutManager(context)
            addItemDecoration(DividerItemDecoration(context, RecyclerView.VERTICAL))
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    // TODO: Ensure lifted state gets cleared when needed, e.g. when navigating to different fragment
                    appBarHost.appBar?.setLifted(canScrollVertically(-1))
                }
            })
        }
    }

    private fun setupFabs() {
        setInitialFabState()
        setupPrimaryFab()
        setupSecondaryFab()
    }

    @Suppress("LongMethod")
    private fun setupPrimaryFab() {
        fabHost.primaryFab?.setOnClickListener {
            when {
                vehiclesAdapter.inEditMode -> {
                    // Already editing, second button press to confirm edits
                    viewModel.updateVehicleList(vehiclesAdapter.currentList)
                    vehiclesAdapter.inEditMode = false
                    if (vehiclesAdapter.currentList.isEmpty()) {
                        showFindVehiclesFab()
                    } else {
                        showEditVehiclesFab()
                    }
                }
                viewModel.allManuallySavedVehicles.value?.size == 0 -> navHost.navigateToSearch()
                else -> {
                    vehiclesAdapter.inEditMode = true
                    showEditFabs()
                }
            }
        }
    }

    private fun setupSecondaryFab() {
        fabHost.secondaryFab?.setOnClickListener {
            // Revert to previous list, allManuallySavedVehicles won't be updated at this point
            val previousItems = viewModel.allManuallySavedVehicles.value
            vehiclesAdapter.submitList(previousItems)
            vehiclesAdapter.inEditMode = false
            showEditVehiclesFab()
        }
    }

    private fun setInitialFabState() {
        fabHost.primaryFab?.slideIn()
        when {
            vehiclesAdapter.inEditMode -> showEditFabs()
            vehiclesAdapter.itemCount == 0 -> showFindVehiclesFab()
            else -> showEditVehiclesFab()
        }
    }

    private fun showEditVehiclesFab() {
        fabHost.primaryFab?.animateFab(R.string.vehicle_edit, R.drawable.ic_edit_grey_24dp, R.color.colorPrimary)
        fabHost.primaryFab?.extend()
        fabHost.secondaryFab?.slideOut()
    }

    private fun showFindVehiclesFab() {
        fabHost.primaryFab?.animateFab(R.string.vehicle_find, R.drawable.ic_search_black_24dp, R.color.colorPrimary)
        fabHost.primaryFab?.extend()
        fabHost.secondaryFab?.slideOut()
    }

    private fun showEditFabs() {
        fabHost.primaryFab?.animateFab(icon = R.drawable.ic_check_white_24dp, color = R.color.successGreen)
        fabHost.primaryFab?.shrink()
        fabHost.secondaryFab?.slideIn()
    }

    private fun requestMotHistory(registrationNumber: String) {
        activity?.supportFragmentManager?.commit {
            addToBackStack(null)
            replace(R.id.fragmentContainer, MotDetailsFragment.newInstance(registrationNumber))
        }
    }
}
