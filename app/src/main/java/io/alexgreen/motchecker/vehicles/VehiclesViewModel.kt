package io.alexgreen.motchecker.vehicles

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * ViewModel used to facilitate with displaying a list of vehicles which have previously been stored.
 */
class VehiclesViewModel @Inject constructor(private val vehicleRepository: VehicleRepository) : ViewModel() {

    /**
     * A LiveData of all the vehicles which were manually saved from the [VehicleRepository].
     */
    val allManuallySavedVehicles = vehicleRepository.getAllManuallySavedVehicles()

    /**
     * A LiveData of recent vehicles from the [VehicleRepository].
     */
    val recentVehicles = vehicleRepository.getRecentVehicles(3)

    /**
     * A LiveData of recent vehicles from the [VehicleRepository].
     */
    val allRecentVehicles by lazy {
        vehicleRepository.getAllRecentVehicles()
    }

    /**
     * Updates the [vehicleRepository]'s vehicles with those in the given [newVehicleList]. Any vehicles from the
     * [vehicleRepository]'s list which are not present within the [newVehicleList] will be marked as unsaved, but not
     * deleted as to preserve recent search history. Remaining vehicles will be updated with their new
     * [Vehicle.itemOrder].
     */
    fun updateVehicleList(newVehicleList: List<Vehicle>) {
        viewModelScope.launch(Dispatchers.IO) {
            val deletedVehicles = allManuallySavedVehicles.value?.minus(newVehicleList)
            deletedVehicles?.forEach { deletedVehicle ->
                vehicleRepository.markVehicleAsUnsaved(deletedVehicle)
            }
            newVehicleList.forEachIndexed { index, vehicle ->
                vehicle.itemOrder = index
                vehicleRepository.updateVehicle(vehicle)
            }
        }
    }
}
