package io.alexgreen.motchecker.vehicles

import androidx.recyclerview.widget.DiffUtil

/**
 * A [DiffUtil.ItemCallback] used when working with lists of [Vehicle].
 */
class VehicleDiffCallback : DiffUtil.ItemCallback<Vehicle>() {

    override fun areItemsTheSame(oldItem: Vehicle, newItem: Vehicle) = oldItem.registration == newItem.registration

    override fun areContentsTheSame(oldItem: Vehicle, newItem: Vehicle) = oldItem == newItem
}
