package io.alexgreen.motchecker.vehicles

import io.alexgreen.motchecker.db.VehicleDao
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Repository to retrieve and store vehicle information using the [vehicleDao].
 */
@Singleton
class VehicleRepository @Inject constructor(private val vehicleDao: VehicleDao) {

    /**
     * Returns whether or not a vehicle with the given [registrationNumber] has been stored in the [vehicleDao]
     * manually by the user.
     */
    @Suppress("ExpressionBodySyntax")
    suspend fun isVehicleManuallySaved(registrationNumber: String): Boolean {
        return vehicleDao.getVehicle(registrationNumber)?.userSavedManually ?: false
    }

    /**
     * Returns the Vehicle with the given [registrationNumber] from tthe [vehicleDao].
     */
    suspend fun getVehicle(registrationNumber: String) = vehicleDao.getVehicle(registrationNumber)

    /**
     * Saves the given [vehicle] in to the database using the [vehicleDao].
     */
    suspend fun saveVehicle(vehicle: Vehicle) = vehicleDao.insertVehicle(vehicle)

    /**
     * Updates the given [vehicle] in the database using the [vehicleDao].
     */
    suspend fun updateVehicle(vehicle: Vehicle) = vehicleDao.updateVehicle(vehicle)

    /**
     * Marks the given [vehicle] as unsaved from the database using the [vehicleDao]. Does not entirely delete as to
     * preserve in recent searches.
     */
    suspend fun markVehicleAsUnsaved(
        vehicle: Vehicle
    ) = vehicleDao.updateVehicle(vehicle.copy(userSavedManually = false))

    /**
     * Returns a LiveData containing a list of all vehicles from the [vehicleDao] which the user manually saved.
     */
    fun getAllManuallySavedVehicles() = vehicleDao.getAllManuallySavedVehicles()

    /**
     * Returns a LiveData containing a list of the most recently saved vehicles, up to an amount of [limit] from the
     * [vehicleDao].
     */
    fun getRecentVehicles(limit: Int) = vehicleDao.getRecentVehicles(limit)

    /**
     * Returns a LiveData containing a list of all the most recently saved vehicles.
     */
    fun getAllRecentVehicles() = vehicleDao.getAllRecentVehicles()
}
