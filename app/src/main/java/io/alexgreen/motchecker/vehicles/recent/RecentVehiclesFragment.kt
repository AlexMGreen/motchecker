package io.alexgreen.motchecker.vehicles.recent

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.support.DaggerFragment
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.vehicles.VehiclesViewModel
import kotlinx.android.synthetic.main.recent_vehicles_fragment.*
import javax.inject.Inject

/**
 * Fragment used to display to the user a list of all their recently searched for Vehicles.
 */
class RecentVehiclesFragment : DaggerFragment(), RecentVehiclesBottomSheetListener {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val vehicleViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(VehiclesViewModel::class.java)
    }
    private lateinit var recentVehiclesAdapter: RecentVehiclesAdapter

    companion object {
        fun newInstance() = RecentVehiclesFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.recent_vehicles_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        allRecentVehiclesClose.setOnClickListener { activity?.supportFragmentManager?.popBackStack() }
        allRecentVehiclesSort.setOnClickListener {
            val bottomSheet = RecentVehiclesBottomSheet.newInstance()
            bottomSheet.show(childFragmentManager, null)
        }
        setupRecentSearchList()
    }

    override fun onSortTypeSelected(sortType: RecentVehiclesSortType) = recentVehiclesAdapter.sortBy(sortType)

    private fun setupRecentSearchList() {
        recentVehiclesAdapter = RecentVehiclesAdapter()
        with(allRecentVehiclesList) {
            adapter = recentVehiclesAdapter
            layoutManager = LinearLayoutManager(context)
        }
        vehicleViewModel.allRecentVehicles.observe(viewLifecycleOwner, Observer { allRecentVehicles ->
            recentVehiclesAdapter.submitList(allRecentVehicles)
        })
    }
}
