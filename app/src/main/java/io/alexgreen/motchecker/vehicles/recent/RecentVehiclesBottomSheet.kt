package io.alexgreen.motchecker.vehicles.recent

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import io.alexgreen.motchecker.R
import kotlinx.android.synthetic.main.recent_vehicles_bottomsheet.*

/**
 * BottomSheet to allow sorting of the recent vehicles list.
 */
class RecentVehiclesBottomSheet : BottomSheetDialogFragment() {
    private lateinit var listener: RecentVehiclesBottomSheetListener

    companion object {
        fun newInstance() = RecentVehiclesBottomSheet()
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog =
        BottomSheetDialog(requireContext(), R.style.BottomSheetDialogTheme)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listener = parentFragment as? RecentVehiclesBottomSheetListener
            ?: throw UnsupportedOperationException("Parent fragment must implement RecentVehiclesBottomSheetListener")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.recent_vehicles_bottomsheet, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        searchDateText.setOnClickListener {
            listener.onSortTypeSelected(RecentVehiclesSortType.SearchDate)
            dismiss()
        }
        makeText.setOnClickListener {
            listener.onSortTypeSelected(RecentVehiclesSortType.Make)
            dismiss()
        }
        modelText.setOnClickListener {
            listener.onSortTypeSelected(RecentVehiclesSortType.Model)
            dismiss()
        }
        registrationText.setOnClickListener {
            listener.onSortTypeSelected(RecentVehiclesSortType.Registration)
            dismiss()
        }
    }
}

/**
 * Listener for RecentVehiclesBottomSheet events.
 */
interface RecentVehiclesBottomSheetListener {
    /**
     * Invoked when a [RecentVehiclesSortType] has been selected.
     */
    fun onSortTypeSelected(sortType: RecentVehiclesSortType)
}
