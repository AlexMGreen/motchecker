package io.alexgreen.motchecker.vehicles

import androidx.room.Entity
import androidx.room.PrimaryKey
import org.threeten.bp.LocalDate
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import org.threeten.bp.format.DateTimeFormatter

@Entity(tableName = "vehicles")
data class Vehicle(
    @PrimaryKey
    val registration: String,
    val make: String,
    val model: String,
    val firstUsedDate: String,
    val fuelType: String,
    val primaryColour: String,
    val userSavedManually: Boolean = false,
    val lastSearchedDate: Long? = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC)
) {
    var itemOrder: Int = Int.MAX_VALUE
}

private val dateFormatter = DateTimeFormatter.ofPattern("yyyy.MM.dd")
/**
 * A [LocalDate] object parsed from the [Vehicle.firstUsedDate]'s original "yyyy.MM.dd" format.
 */
val Vehicle.parsedFirstUsedDate: LocalDate
    get() = LocalDate.parse(firstUsedDate, dateFormatter)
