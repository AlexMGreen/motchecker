package io.alexgreen.motchecker.vehicles.saved

import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import io.alexgreen.motchecker.extensions.formatDayMonthYear
import io.alexgreen.motchecker.extensions.visibleIf
import io.alexgreen.motchecker.vehicles.Vehicle
import io.alexgreen.motchecker.vehicles.VehicleDiffCallback
import io.alexgreen.motchecker.vehicles.parsedFirstUsedDate
import io.alexgreen.motchecker.vehicles.saved.SavedVehiclesAdapter.SavedVehicleViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_saved_vehicle.*
import java.util.Collections

/**
 * [ListAdapter] used for displaying a list of [Vehicle] which have been manually saved by the user.
 */
class SavedVehiclesAdapter(
    private val vehicleListListener: VehicleListListener
) : ListAdapter<Vehicle, SavedVehicleViewHolder>(VehicleDiffCallback()) {

    /**
     * Whether or not the adapter is in edit mode. When true, items held by the adapter will display additional icons
     * to allow for editing, and normal item clicks will be disabled.
     */
    var inEditMode: Boolean = false
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = SavedVehicleViewHolder(
        LayoutInflater.from(parent.context).inflate(io.alexgreen.motchecker.R.layout.item_saved_vehicle, parent, false)
    ).apply {
        setupItemClick()
        setupDeleteClick()
        setupReorderClick()
    }

    override fun onBindViewHolder(holder: SavedVehicleViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    /**
     * Reorders and resubmits the list when a row has been moved from [fromPosition] to [toPosition].
     */
    fun onRowMoved(fromPosition: Int, toPosition: Int) {
        val newList = currentList.toMutableList()
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(newList, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(newList, i, i - 1)
            }
        }
        submitList(newList)
    }

    private fun SavedVehicleViewHolder.setupItemClick() {
        itemView.setOnClickListener {
            if (adapterPosition == RecyclerView.NO_POSITION) {
                return@setOnClickListener
            }
            if (!inEditMode) {
                vehicleListListener.onItemClicked(getItem(adapterPosition))
            }
        }
    }

    private fun SavedVehicleViewHolder.setupDeleteClick() {
        vehicleDelete.setOnClickListener {
            if (adapterPosition == RecyclerView.NO_POSITION) {
                return@setOnClickListener
            }
            submitList(currentList - getItem(adapterPosition))
        }
    }

    private fun SavedVehicleViewHolder.setupReorderClick() {
        vehicleReorder.setOnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                vehicleListListener.onItemDragged(this)
            }
            false
        }
    }

    /**
     * [RecyclerView.ViewHolder] for binding a [Vehicle] to a view.
     */
    inner class SavedVehicleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View?
            get() = itemView

        /**
         * Binds the given [vehicle] to an [itemView].
         */
        fun bind(vehicle: Vehicle) {
            vehicleMake.text = vehicle.make
            vehicleModel.text = vehicle.model.toLowerCase().split(' ').joinToString(separator = " ") { it.capitalize() }
            vehicleColorText.text = vehicle.primaryColour
            vehicleFirstUsedDate.text = vehicle.parsedFirstUsedDate.formatDayMonthYear()
            vehicleRegistration.text = vehicle.registration
            vehicleDelete.visibleIf(shouldFade = true) { inEditMode }
            vehicleReorder.visibleIf(shouldFade = true) { inEditMode }
        }
    }
}

/**
 * A listener to be informed about events from the vehicle list.
 */
interface VehicleListListener {
    /**
     * Invoked upon an item being dragged, passing along the item's [viewHolder].
     */
    fun onItemDragged(viewHolder: SavedVehicleViewHolder)

    /**
     * Invoked upon an item being clicked, passing along the clicked [vehicle].
     */
    fun onItemClicked(vehicle: Vehicle)
}
