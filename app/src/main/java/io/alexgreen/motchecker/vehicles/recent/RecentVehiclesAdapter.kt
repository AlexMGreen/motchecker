package io.alexgreen.motchecker.vehicles.recent

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.vehicles.Vehicle
import io.alexgreen.motchecker.vehicles.VehicleDiffCallback
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_recent_vehicle.*

/**
 * Adapter used for displaying a list of [Vehicle] which have been recently searched for by the user.
 */
class RecentVehiclesAdapter(
    private val onVehicleClicked: ((Vehicle) -> Unit)? = null
) : ListAdapter<Vehicle, RecentVehiclesAdapter.RecentVehicleViewHolder>(VehicleDiffCallback()) {

    /**
     * Sorts the list by the given [RecentVehiclesSortType].
     */
    fun sortBy(sortType: RecentVehiclesSortType) {
        val newList = when (sortType) {
            RecentVehiclesSortType.SearchDate -> currentList.sortedByDescending { it.lastSearchedDate }
            RecentVehiclesSortType.Make -> currentList.sortedBy { it.make }
            RecentVehiclesSortType.Model -> currentList.sortedBy { it.model }
            RecentVehiclesSortType.Registration -> currentList.sortedBy { it.registration }
        }
        submitList(newList)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentVehicleViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return RecentVehicleViewHolder(inflater.inflate(R.layout.item_recent_vehicle, parent, false)).apply {
            itemView.setOnClickListener {
                if (adapterPosition == RecyclerView.NO_POSITION) {
                    return@setOnClickListener
                }
                onVehicleClicked?.invoke(getItem(adapterPosition))
            }
        }
    }

    override fun onBindViewHolder(holder: RecentVehicleViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    /**
     * [RecyclerView.ViewHolder] for binding a [Vehicle] to a view.
     */
    inner class RecentVehicleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View?
            get() = itemView

        /**
         * Binds the given [vehicle] to an [itemView].
         */
        fun bind(vehicle: Vehicle) {
            vehicleMake.text = vehicle.make
            vehicleModel.text = vehicle.model.toLowerCase().split(' ').joinToString(separator = " ") { it.capitalize() }
            vehicleRegistration.text = vehicle.registration
        }
    }
}
