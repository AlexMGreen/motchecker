package io.alexgreen.motchecker

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider
import javax.inject.Singleton

/**
 * Factory used when retrieving [ViewModel]s using [ViewModelProvider].
 */
@Suppress("UNCHECKED_CAST")
@Singleton
class ViewModelFactory @Inject constructor(
    private val creators: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        var creator = creators[modelClass]
        if (creator == null) {
            creator = creators
                .entries
                .firstOrNull { modelClass.isAssignableFrom(it.key) }
                ?.value
                ?: throw IllegalArgumentException("unknown model class $modelClass")
        }

        return creator.get() as T
    }
}
