package io.alexgreen.motchecker.extensions

import android.content.res.Resources

/**
 * Converts the value to DP.
 */
fun Int.toDp(): Int = (this / Resources.getSystem().displayMetrics.density).toInt()

/**
 * Converts the value to DP.
 */
fun Int.toPx(): Int = (this * Resources.getSystem().displayMetrics.density).toInt()
