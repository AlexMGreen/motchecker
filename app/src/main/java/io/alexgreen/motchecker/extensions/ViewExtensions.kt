@file:Suppress("TooManyFunctions")
package io.alexgreen.motchecker.extensions

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ObjectAnimator
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.inputmethod.InputMethodManager
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.transition.AutoTransition
import androidx.transition.TransitionManager
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton

/**
 * Shows the view.
 */
fun View.show() {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
}

/**
 * Hides the view, setting visibility to [View.GONE].
 */
fun View.hide() {
    if (visibility != View.GONE) {
        visibility = View.GONE
    }
}

/**
 * If [condition] then shows the view, otherwise hides the view. If [shouldFade] then visibility changes will be
 * performed using a fade animation.
 */
fun View.visibleIf(shouldFade: Boolean = false, condition: () -> Boolean) {
    if (condition()) {
        if (shouldFade) {
            fadeIn()
        } else {
            show()
        }
    } else {
        if (shouldFade) {
            fadeOut()
        } else {
            hide()
        }
    }
}

/**
 * Fades the view in over the given [durationMillis] after an initial [startDelayMillis]. Alpha will
 * initially be set to 0 and visibility set to [View.VISIBLE] prior to starting the animation.
 */
fun View.fadeIn(durationMillis: Long = 250L, startDelayMillis: Long = 0L) {
    alpha = 0f
    show()
    animate()
        .setListener(null)
        .alpha(1f)
        .setStartDelay(startDelayMillis)
        .duration = durationMillis
}

/**
 * Fades the view out over the given [durationMillis] after an initial [startDelayMillis].
 * Visibility will be set to [View.GONE] upon completion of the animation.
 */
fun View.fadeOut(durationMillis: Long = 250L, startDelayMillis: Long = 0L) {
    animate()
        .alpha(0f)
        .setStartDelay(startDelayMillis)
        .setDuration(durationMillis)
        .setListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                visibility = View.GONE
            }
        })
}

/**
 * Hides the soft keyboard.
 */
fun View.hideKeyboard() = (context.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
    ?.hideSoftInputFromWindow(windowToken, 0)

/**
 * Animates the CardView to the given [elevation] from its existing elevation.
 */
fun CardView.elevateTo(elevation: Float) {
    ObjectAnimator.ofFloat(this, "cardElevation", elevation).apply {
        duration = 200
        start()
    }
}

/**
 * Animates the [ExtendedFloatingActionButton] whilst optionally changing the [text], [icon], and [color] if non-null
 * values are passed for these.
 */
fun ExtendedFloatingActionButton.animateFab(
    @StringRes text: Int? = null,
    @DrawableRes icon: Int? = null,
    @ColorRes color: Int? = null
) {
    (parent as? ViewGroup)?.let { viewGroup ->
        TransitionManager.beginDelayedTransition(viewGroup, AutoTransition().setDuration(100).addTarget(this))
    }
    if (text != null) {
        setText(text)
    }
    if (icon != null) {
        setIcon(ContextCompat.getDrawable(context, icon))
    }
    if (color != null) {
        setBackgroundColor(ContextCompat.getColor(context, color))
    }
}

/**
 * Slides the [ExtendedFloatingActionButton] downwards to an offscreen position.
 */
fun ExtendedFloatingActionButton.slideOut() {
    animate()
        .translationY(height + 96.toPx().toFloat())
        .setDuration(100)
        .interpolator = AccelerateDecelerateInterpolator()
}

/**
 * Slides the [ExtendedFloatingActionButton] upwards from an offscreen position to its normal Y position. Visibility
 * will be set to VISIBLE if needed.
 */
fun ExtendedFloatingActionButton.slideIn() {
    show()
    animate()
        .translationY(0f)
        .setDuration(100)
        .interpolator = AccelerateDecelerateInterpolator()
}
