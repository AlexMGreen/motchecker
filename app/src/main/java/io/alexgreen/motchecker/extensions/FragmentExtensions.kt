package io.alexgreen.motchecker.extensions

import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment

/**
 * Returns the non-null extra from the [Fragment]'s arguments with the given [key], returning [default] if this is not
 * present; an [IllegalArgumentException] will be thrown if both of these are null.
 */
inline fun <reified T : Any> Fragment.extraNotNull(key: String, default: T? = null) = lazy {
    val value = arguments?.get(key)
    requireNotNull(if (value is T) value else default) { key }
}

/**
 * A [Handler] using [Looper.getMainLooper].
 */
val Fragment.mainHandler by lazy { Handler(Looper.getMainLooper()) }
