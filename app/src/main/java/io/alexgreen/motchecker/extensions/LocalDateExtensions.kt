package io.alexgreen.motchecker.extensions

import org.threeten.bp.LocalDate
import org.threeten.bp.format.DateTimeFormatter

/**
 * Returns the [LocalDate] as a [String] of the form "dd MMM yyyy" (e.g. "13 Nov 2010").
 */
fun LocalDate.formatDayMonthYear(): String = dateFormatter.format(this)

private val dateFormatter = DateTimeFormatter.ofPattern("dd MMM yyyy")

/**
 * Returns the [LocalDate] as a [String] of the form "MM-yyyy" (e.g. "11-2010").
 */
fun LocalDate.formatMonthYear(): String = monthYearFormatter.format(this)

private val monthYearFormatter = DateTimeFormatter.ofPattern("MM-yyyy")
