package io.alexgreen.motchecker.di.modules

import android.content.Context
import androidx.room.Room
import dagger.Module
import dagger.Provides
import io.alexgreen.motchecker.db.VehicleDatabase
import javax.inject.Singleton

@Module
object VehicleModule {

    @Singleton
    @Provides
    @JvmStatic
    fun providesVehicleDatabase(context: Context) = Room
        .databaseBuilder(context, VehicleDatabase::class.java, "VehicleDatabase.db")
        .build()

    @Singleton
    @Provides
    @JvmStatic
    fun providesVehicleDao(vehicleDatabase: VehicleDatabase) = vehicleDatabase.vehicleDao()
}
