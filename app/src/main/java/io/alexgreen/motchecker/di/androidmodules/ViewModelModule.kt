package io.alexgreen.motchecker.di.androidmodules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import io.alexgreen.motchecker.ViewModelFactory
import io.alexgreen.motchecker.motdetails.MotDetailsViewModel
import io.alexgreen.motchecker.search.CameraSearchViewModel
import io.alexgreen.motchecker.vehicles.VehiclesViewModel
import kotlin.reflect.KClass

@Module
internal abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(MotDetailsViewModel::class)
    internal abstract fun bindsMotDetailsViewModel(viewModel: MotDetailsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(VehiclesViewModel::class)
    internal abstract fun bindsVehiclesViewModel(viewModel: VehiclesViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CameraSearchViewModel::class)
    internal abstract fun bindsCameraSearchViewModel(viewModel: CameraSearchViewModel): ViewModel

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @MustBeDocumented
    @Target(
        AnnotationTarget.FUNCTION,
        AnnotationTarget.PROPERTY_GETTER,
        AnnotationTarget.PROPERTY_SETTER
    )
    @Retention(AnnotationRetention.RUNTIME)
    @MapKey
    private annotation class ViewModelKey(val value: KClass<out ViewModel>)
}
