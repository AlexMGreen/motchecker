package io.alexgreen.motchecker.di.androidmodules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.alexgreen.motchecker.motdetails.MotDetailsFragment
import io.alexgreen.motchecker.search.CameraSearchFragment
import io.alexgreen.motchecker.search.SearchFragment
import io.alexgreen.motchecker.vehicles.recent.RecentVehiclesFragment
import io.alexgreen.motchecker.vehicles.saved.SavedVehiclesFragment

@Module
internal abstract class FragmentModule {

    @ContributesAndroidInjector
    internal abstract fun contributesSearchFragment(): SearchFragment

    @ContributesAndroidInjector
    internal abstract fun contributesCameraSearchFragment(): CameraSearchFragment

    @ContributesAndroidInjector
    internal abstract fun contributesRecentVehiclesFragment(): RecentVehiclesFragment

    @ContributesAndroidInjector
    internal abstract fun contributesMotDetailsFragment(): MotDetailsFragment

    @ContributesAndroidInjector
    internal abstract fun contributesSavedVehiclesFragment(): SavedVehiclesFragment
}
