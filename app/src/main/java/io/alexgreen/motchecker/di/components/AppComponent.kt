package io.alexgreen.motchecker.di.components

import android.app.Application
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import io.alexgreen.motchecker.App
import io.alexgreen.motchecker.di.androidmodules.ActivityModule
import io.alexgreen.motchecker.di.androidmodules.FragmentModule
import io.alexgreen.motchecker.di.androidmodules.ViewModelModule
import io.alexgreen.motchecker.di.modules.AppModule
import io.alexgreen.motchecker.di.modules.MotModule
import io.alexgreen.motchecker.di.modules.TimeModule
import io.alexgreen.motchecker.di.modules.VehicleModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        ActivityModule::class,
        FragmentModule::class,
        ViewModelModule::class,
        AndroidSupportInjectionModule::class,
        MotModule::class,
        TimeModule::class,
        VehicleModule::class
    ]
)
internal interface AppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun create(application: Application): Builder

        fun build(): AppComponent
    }
}
