package io.alexgreen.motchecker.di.modules

import dagger.Module
import dagger.Provides
import org.threeten.bp.Clock
import javax.inject.Singleton

@Module
object TimeModule {
    @Singleton
    @Provides
    @JvmStatic
    fun providesClock(): Clock = Clock.systemDefaultZone()
}
