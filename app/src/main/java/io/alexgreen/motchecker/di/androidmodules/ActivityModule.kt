package io.alexgreen.motchecker.di.androidmodules

import dagger.Module
import dagger.android.ContributesAndroidInjector
import io.alexgreen.motchecker.MainActivity

@Module
internal abstract class ActivityModule {

    @ContributesAndroidInjector
    internal abstract fun contributesMainActivity(): MainActivity
}
