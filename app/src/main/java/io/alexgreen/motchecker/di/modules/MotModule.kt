package io.alexgreen.motchecker.di.modules

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import dagger.Module
import dagger.Provides
import io.alexgreen.motchecker.BuildConfig
import io.alexgreen.motchecker.mot.MotService
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton

@Module
object MotModule {
    private val authInterceptor = Interceptor { chain ->
        val request = chain.request()
            .newBuilder()
            .addHeader("Content-type", "application/json")
            .addHeader("x-api-key", BuildConfig.MOT_API_KEY)
            .build()

        chain.proceed(request)
    }

    private val motClient = OkHttpClient().newBuilder()
        .addInterceptor(authInterceptor)
        .addInterceptor(HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY })
        .build()

    @Singleton
    @Provides
    @JvmStatic
    fun providesMotService(): MotService = Retrofit.Builder()
        .client(motClient)
        // TODO: Allow updating base URL without app update
        .baseUrl("https://beta.check-mot.service.gov.uk")
        .addConverterFactory(MoshiConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .build()
        .create(MotService::class.java)
}
