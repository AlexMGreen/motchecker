package io.alexgreen.motchecker.util

import android.content.SharedPreferences
import dagger.Reusable
import javax.inject.Inject

/**
 * Helper class for interacting with [SharedPreferences].
 */
@Reusable
class SharedPreferencesHelper @Inject constructor(private val sharedPreferences: SharedPreferences) {

    /**
     * Sets the given [key] [value] pair in to [SharedPreferences], updating the existing value if present.
     */
    operator fun SharedPreferences.set(key: String, value: Any?) {
        when (value) {
            is String? -> edit { it.putString(key, value) }
            is Int -> edit { it.putInt(key, value) }
            is Boolean -> edit { it.putBoolean(key, value) }
            is Float -> edit { it.putFloat(key, value) }
            is Long -> edit { it.putLong(key, value) }
            else -> throw UnsupportedOperationException()
        }
    }

    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor = this.edit()
        operation(editor)
        editor.apply()
    }
}
