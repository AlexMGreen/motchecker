package io.alexgreen.motchecker.views

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

/**
 * ViewPager that allows setting of a [SwipeDirection] as an [allowedSwipeDirection], preventing
 * user swipes in a given direction when desired.
 */
class SwipeDirectionViewPager : ViewPager {
    /**
     * The [SwipeDirection] at which the ViewPager will allow swiping to.
     */
    var allowedSwipeDirection = SwipeDirection.ALL
    private var initialXValue = 0f

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    override fun onTouchEvent(ev: MotionEvent) = if (isSwipeAllowed(ev)) super.onTouchEvent(ev) else false

    override fun onInterceptTouchEvent(ev: MotionEvent) = if (isSwipeAllowed(ev)) {
        super.onInterceptTouchEvent(ev)
    } else {
        false
    }

    private fun isSwipeAllowed(motionEvent: MotionEvent): Boolean = when {
        allowedSwipeDirection == SwipeDirection.ALL -> true
        allowedSwipeDirection == SwipeDirection.NONE -> false
        motionEvent.action == MotionEvent.ACTION_DOWN -> {
            initialXValue = motionEvent.x
            true
        }
        motionEvent.action == MotionEvent.ACTION_MOVE -> {
            val xDiff = motionEvent.x - initialXValue
            if (xDiff > 0 && allowedSwipeDirection == SwipeDirection.RIGHT) {
                // Swiping from right to left, with allowed direction of [SwipeDirection.RIGHT] disallow
                false
                // Swiping from left to right, with allowed direction of [SwipeDirection.LEFT] disallow
            } else !(xDiff < 0 && allowedSwipeDirection == SwipeDirection.LEFT)
        }
        else -> true
    }
}

/**
 * Defines all possible swipe directions to be set on the [SwipeDirectionViewPager].
 */
enum class SwipeDirection {
    ALL, LEFT, RIGHT, NONE
}
