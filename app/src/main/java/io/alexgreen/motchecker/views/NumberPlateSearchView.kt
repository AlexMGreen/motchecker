package io.alexgreen.motchecker.views

import android.content.Context
import android.text.InputFilter
import android.text.InputType
import android.util.AttributeSet
import android.view.Gravity
import android.view.MotionEvent
import android.widget.EditText
import androidx.appcompat.widget.AppCompatEditText
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import io.alexgreen.motchecker.R

/**
 * A Number Plate styled [EditText] used for searching.
 */
class NumberPlateSearchView : AppCompatEditText {
    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    init {
        setupTextProperties()
        background = ContextCompat.getDrawable(context, R.drawable.number_plate_background)
        gravity = Gravity.CENTER
    }

    /**
     * Whether or not the search view is currently editable.
     */
    var isEditable: Boolean = true
        set(value) {
            field = value
            isCursorVisible = value
            isPressed = value
            isFocusable = value
            if (!value) clearFocus()
        }

    /**
     * Sets the [onClick] action to be invoked upon receiving clicks on the NumberPlateSearchView's icon.
     */
    fun setIconClickListener(onClick: () -> Unit) = setOnTouchListener { _, event ->
        if (event.action == MotionEvent.ACTION_UP &&
            event.rawX <= totalPaddingLeft + compoundDrawables.first().bounds.width()
        ) {
            onClick()
            return@setOnTouchListener true
        }
        return@setOnTouchListener false
    }

    override fun isSuggestionsEnabled() = false

    private fun setupTextProperties() {
        typeface = ResourcesCompat.getFont(context, R.font.uk_number_plate)
        isAllCaps = true
        inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
        inputType = InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS or InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD
        filters += InputFilter.AllCaps()
        setTextColor(ContextCompat.getColor(context, R.color.number_plate_search_color_selector))
    }
}
