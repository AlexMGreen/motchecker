package io.alexgreen.motchecker.search

import android.Manifest
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.PermissionChecker
import androidx.core.content.PermissionChecker.checkSelfPermission
import androidx.core.os.postDelayed
import androidx.core.util.valueIterator
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.commit
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.transition.ChangeBounds
import androidx.transition.Transition
import com.google.android.gms.vision.CameraSource
import com.google.android.gms.vision.Detector
import com.google.android.gms.vision.text.TextBlock
import com.google.android.gms.vision.text.TextRecognizer
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_COLLAPSED
import com.google.android.material.bottomsheet.BottomSheetBehavior.STATE_HALF_EXPANDED
import dagger.android.support.DaggerFragment
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.extensions.fadeIn
import io.alexgreen.motchecker.extensions.fadeOut
import io.alexgreen.motchecker.extensions.mainHandler
import io.alexgreen.motchecker.motdetails.MotDetailsFragment
import io.alexgreen.motchecker.motdetails.MotDetailsListener
import kotlinx.android.synthetic.main.camera_search_fragment.*
import timber.log.Timber
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit
import javax.inject.Inject

private const val CAMERA_PERMISSION_CODE = 126
private const val LAST_RECEIVED_DETECTION = "lastReceivedDetection"

/**
 * Activity to allow searching of vehicles using the Camera.
 */
@Suppress("TooManyFunctions")
class CameraSearchFragment : DaggerFragment(), MotDetailsListener {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(CameraSearchViewModel::class.java)
    }
    private var cameraSource: CameraSource? = null
    private lateinit var bottomSheetBehavior: BottomSheetBehavior<View>
    private val changeBoundsTransition: Transition
        get() = ChangeBounds().setInterpolator(DecelerateInterpolator()).setDuration(150)
    private val scheduler = Executors.newScheduledThreadPool(1)
    private var checkDetectionsTask: ScheduledFuture<*>? = null
    private var lastReceivedDetection: String? = null // TODO: Save & restore

    companion object {
        fun newInstance() = CameraSearchFragment().apply {
            sharedElementEnterTransition = changeBoundsTransition
            enterTransition = changeBoundsTransition
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.camera_search_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lastReceivedDetection = savedInstanceState?.getString(LAST_RECEIVED_DETECTION)
        cameraSearchClose.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }
        incorrectRegButton.setOnClickListener {
            lastReceivedDetection?.let {
                viewModel.denySuggestion(it)
            }
            onMotDetailsClosed()
        }
        setupBottomSheet()
        if (checkSelfPermission(requireContext(), Manifest.permission.CAMERA) == PermissionChecker.PERMISSION_GRANTED) {
            createCameraSource()
        } else {
            requestCameraPermission()
        }
    }

    private fun setupBottomSheet() {
        bottomSheetBehavior = BottomSheetBehavior.from(cameraSearchResultSheet)
        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                val params = sheetGuideline?.layoutParams as? ConstraintLayout.LayoutParams
                params?.guideBegin = bottomSheet.top
                sheetGuideline.layoutParams = params
            }

            override fun onStateChanged(bottomSheet: View, newState: Int) {
            }
        })
    }

    override fun onResume() {
        super.onResume()
        if (bottomSheetBehavior.state == STATE_COLLAPSED) {
            startCamera()
        }
    }

    override fun onPause() {
        super.onPause()
        stopCamera()
    }

    override fun onDestroy() {
        super.onDestroy()
        cameraPreview?.release()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(LAST_RECEIVED_DETECTION, lastReceivedDetection)
        super.onSaveInstanceState(outState)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            Timber.d("Camera permission granted, starting camera source")
            createCameraSource()
            startCamera()
            return
        }
    }

    override fun onMotDetailsRetrieved() {
        bottomSheetBehavior.state = STATE_HALF_EXPANDED
        mainHandler.postDelayed(delayInMillis = 500) {
            stopCamera()
        }
    }

    override fun onMotDetailsClosed() {
        bottomSheetBehavior.state = STATE_COLLAPSED
        cameraNumberPlateSearchCard.fadeIn()
        childFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        incorrectRegButton.fadeOut()
        mainHandler.postDelayed(delayInMillis = 500) {
            startCamera()
        }
    }

    private fun requestCameraPermission() {
        Timber.w("Camera permission not granted, requesting permission")
        requestPermissions(arrayOf(Manifest.permission.CAMERA), CAMERA_PERMISSION_CODE)
    }

    private fun createCameraSource() {
        cameraSource = CameraSource.Builder(context?.applicationContext, setupRecognizer())
            .setFacing(CameraSource.CAMERA_FACING_BACK)
            .setRequestedFps(2.0f)
            .setAutoFocusEnabled(true)
            .build()
    }

    private fun startMonitoringDetections() {
        checkDetectionsTask = scheduler.scheduleAtFixedRate(::checkDetections, 2500, 2500, TimeUnit.MILLISECONDS)
    }

    private fun stopMonitoringDetections() {
        checkDetectionsTask?.cancel(true)
    }

    private fun checkDetections() {
        mainHandler.post {
            viewModel.getMostLikelyRegistration()?.let { mostLikelyReg ->
                Timber.d(
                    "Checking detections, most likely reg is: $mostLikelyReg, last received is: $lastReceivedDetection"
                )
                if (mostLikelyReg == lastReceivedDetection) return@let
                lastReceivedDetection = mostLikelyReg
                showMotDetails(mostLikelyReg)
            }
        }
    }

    private fun showMotDetails(mostLikelyReg: String) {
        Timber.d("Showing MOT details for: $mostLikelyReg")
        childFragmentManager.commit {
            replace(R.id.cameraSearchResultSheet, MotDetailsFragment.newInstance(mostLikelyReg))
            addToBackStack(null)
        }
        cameraNumberPlateSearchCard.fadeOut(100)
        incorrectRegButton.fadeIn()
    }

    private fun setupRecognizer(): TextRecognizer {
        val textRecognizer = TextRecognizer.Builder(context?.applicationContext).build()
        textRecognizer.setProcessor(object : Detector.Processor<TextBlock> {
            override fun release() {}

            override fun receiveDetections(detections: Detector.Detections<TextBlock>?) {
                detections?.detectedItems?.valueIterator()?.forEach {
                    viewModel.receiveDetection(it.value)
                }
            }
        })

        if (!textRecognizer.isOperational) {
            Timber.w("Detector dependencies are not yet available.")
            val lowStorageFilter = IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW)
            val hasLowStorage = activity?.registerReceiver(null, lowStorageFilter) != null
            if (hasLowStorage) {
                Timber.w("Low storage, TextRecognizer not operational")
            }
        }

        return textRecognizer
    }

    private fun startCamera() {
        val hasPermission = checkSelfPermission(requireContext(), Manifest.permission.CAMERA)
        if (hasPermission == PackageManager.PERMISSION_GRANTED) {
            cameraPreview.start(cameraSource)
            startMonitoringDetections()
        } else {
            cameraSource?.release()
            cameraSource = null
        }
    }

    private fun stopCamera() {
        cameraPreview.stop()
        stopMonitoringDetections()
    }
}
