package io.alexgreen.motchecker.search

import androidx.lifecycle.ViewModel
import timber.log.Timber
import java.util.Locale
import javax.inject.Inject

@Suppress("MaxLineLength")
private const val REGISTRATION_PATTERN =
    "(^[A-Z]{2}[0-9]{2} [A-Z]{3}\$)|(^[A-Z][0-9]{1,3} [A-Z]{3}\$)|(^[A-Z]{3} [0-9]{1,3}[A-Z]\$)|(^[0-9]{1,4} [A-Z]{1,2}\$)|(^[0-9]{1,3} [A-Z]{1,3}\$)|(^[A-Z]{1,2} [0-9]{1,4}\$)|(^[A-Z]{1,3} [0-9]{1,3}\$)"

/**
 * ViewModel to facilitate with searching for vehicles using the Camera.
 */
class CameraSearchViewModel @Inject constructor() : ViewModel() {
    private val registrationRegex = REGISTRATION_PATTERN.toRegex()
    private val newline = "\\n".toRegex()
    private val detections = mutableListOf<String>()
    private val deniedSuggestions = mutableListOf<String>()

    /**
     * Receives a detection, and checks to see if its a valid registration number, adding it to [detections] if so.
     */
    fun receiveDetection(detection: String) {
        // TODO MOTC-60: O can be used for area code (Oxford) and Z can be used for random letters
        //  ( https://en.wikipedia.org/wiki/Vehicle_registration_plates_of_the_United_Kingdom )
        val normalised = detection
            .toUpperCase(Locale.getDefault())
            .replace(newline, " ")
            .replace("I", "1")
            .replace("O", "0")
            .replace("Z", "2")
        if (normalised.matches(registrationRegex)) {
            Timber.d("Detected numberplate: $normalised")
            detections.add(normalised)
        } else {
            Timber.d("Not a numberplate: $normalised")
        }
    }

    /**
     * Mark the [suggestion] as denied, placing it in a blacklist preventing the value being given as the
     * most likely registration again in the future.
     */
    fun denySuggestion(suggestion: String) = deniedSuggestions.add(suggestion)

    /**
     * Gets the registration from [detections] which has been determined as the most likely registration from the
     * attempts at detection, based on the occurrence at which the registration appeared via [receiveDetection]. If no
     * valid registrations have been passed to [receiveDetection], or if all valid detections passed have subsequently
     * been denied via [denySuggestion] then null will be returned.
     */
    fun getMostLikelyRegistration() = detections
        .groupingBy { it }
        .eachCount()
        .toList()
        .sortedByDescending { (_, value) -> value }
        .firstOrNull { !deniedSuggestions.contains(it.first) }?.first
}
