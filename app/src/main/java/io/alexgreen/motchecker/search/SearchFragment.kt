package io.alexgreen.motchecker.search

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.view.inputmethod.EditorInfo
import androidx.fragment.app.commit
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.ChangeBounds
import androidx.transition.Transition
import dagger.android.support.DaggerFragment
import io.alexgreen.motchecker.AppBarHost
import io.alexgreen.motchecker.FabHost
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.extensions.hideKeyboard
import io.alexgreen.motchecker.extensions.slideOut
import io.alexgreen.motchecker.extensions.visibleIf
import io.alexgreen.motchecker.motdetails.MotDetailsFragment
import io.alexgreen.motchecker.vehicles.VehiclesViewModel
import io.alexgreen.motchecker.vehicles.recent.RecentVehiclesAdapter
import io.alexgreen.motchecker.vehicles.recent.RecentVehiclesFragment
import kotlinx.android.synthetic.main.search_fragment.*
import javax.inject.Inject

/**
 * Fragment used to allow the user to search for a vehicle's MOT history. If the search was successful, details will be
 * displayed using the [MotDetailsFragment].
 */
class SearchFragment : DaggerFragment() {
    private val changeBoundsTransition: Transition
        get() = ChangeBounds().setInterpolator(DecelerateInterpolator()).setDuration(150)
    private lateinit var fabHost: FabHost
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val vehicleViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(VehiclesViewModel::class.java)
    }
    private lateinit var appBarHost: AppBarHost

    companion object {
        fun newInstance() = SearchFragment()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        fabHost = parentFragment as? FabHost
            ?: throw UnsupportedOperationException("Parent fragment must implement FabHost")
        appBarHost = parentFragment as? AppBarHost
            ?: throw UnsupportedOperationException("Parent fragment must implement AppBarHost")
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.search_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sharedElementReturnTransition = changeBoundsTransition
        exitTransition = changeBoundsTransition
        numberPlateSearch.imeOptions = EditorInfo.IME_ACTION_SEARCH
        fabHost.primaryFab?.visibility = View.GONE
        setupClickListeners()
        setupRecentSearchList()
        searchScrollView.viewTreeObserver.addOnScrollChangedListener {
            // TODO: Ensure lifted state gets cleared when needed, e.g. when navigating to different fragment
            appBarHost.appBar?.setLifted(searchScrollView.canScrollVertically(-1))
        }
    }

    override fun onResume() {
        super.onResume()
        fabHost.primaryFab?.slideOut()
        fabHost.secondaryFab?.slideOut()
    }

    private fun setupClickListeners() {
        searchButton.setOnClickListener { requestMotHistory() }
        cameraSearchCard.setOnClickListener { openCamera() }
        numberPlateSearch.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                requestMotHistory()
            }
            return@setOnEditorActionListener false
        }
        seeAllButton.setOnClickListener {
            activity?.supportFragmentManager?.commit {
                addToBackStack(null)
                replace(R.id.fragmentContainer, RecentVehiclesFragment.newInstance())
            }
        }
    }

    private fun requestMotHistory(registrationNumber: String = numberPlateSearch.text.toString()) {
        numberPlateSearch.hideKeyboard()
        activity?.supportFragmentManager?.commit {
            addSharedElement(numberPlateSearchCard, "numberPlateSearchTransition")
            // TODO: Manage backstack for each bottom nav location
            addToBackStack(null)
            replace(R.id.fragmentContainer, MotDetailsFragment.newInstance(registrationNumber))
        }
    }

    private fun setupRecentSearchList() {
        val recentVehiclesAdapter = RecentVehiclesAdapter { clickedVehicle ->
            requestMotHistory(clickedVehicle.registration)
        }
        with(recentVehiclesList) {
            adapter = recentVehiclesAdapter
            layoutManager = LinearLayoutManager(context)
        }
        vehicleViewModel.recentVehicles.observe(viewLifecycleOwner, Observer { recentVehicles ->
            recentSearchesCard.visibleIf { recentVehicles.isNotEmpty() }
            recentVehiclesAdapter.submitList(recentVehicles)
        })
    }

    private fun openCamera() {
        activity?.supportFragmentManager?.commit {
            addSharedElement(cameraSearchCard, "cameraSearchTransition")
            addToBackStack(null)
            replace(R.id.fragmentContainer, CameraSearchFragment.newInstance())
        }
    }
}
