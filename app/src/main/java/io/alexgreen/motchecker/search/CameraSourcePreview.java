package io.alexgreen.motchecker.search;

import android.Manifest;
import android.content.Context;
import android.content.res.Configuration;
import android.util.AttributeSet;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import androidx.annotation.RequiresPermission;
import com.google.android.gms.common.images.Size;
import com.google.android.gms.vision.CameraSource;
import timber.log.Timber;

import java.io.IOException;

/**
 * A ViewGroup, displaying a CameraSource on a SurfaceView, maintaining optimal aspect ratio while fitting the requested
 * size.
 */
public class CameraSourcePreview extends ViewGroup {

    private ScaleType scaleType = ScaleType.FIT_CROP;
    private Context mContext;
    private SurfaceView mSurfaceView;
    private boolean mStartRequested;
    private boolean mSurfaceAvailable;
    private CameraSource mCameraSource;

    public CameraSourcePreview(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mStartRequested = false;
        mSurfaceAvailable = false;

        mSurfaceView = new SurfaceView(context);
        mSurfaceView.getHolder().addCallback(new SurfaceCallback());
        addView(mSurfaceView);
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    public void start(CameraSource cameraSource) throws IOException, SecurityException {
        if (cameraSource == null) {
            stop();
        }

        mCameraSource = cameraSource;

        if (mCameraSource != null) {
            mStartRequested = true;
            startIfReady();
        }
    }

    public void stop() {
        if (mCameraSource != null) {
            mCameraSource.stop();
        }
    }

    public void release() {
        if (mCameraSource != null) {
            mCameraSource.release();
            mCameraSource = null;
        }
    }

    @RequiresPermission(Manifest.permission.CAMERA)
    private void startIfReady() throws IOException, SecurityException {
        if (mStartRequested && mSurfaceAvailable) {
            mCameraSource.start(mSurfaceView.getHolder());
            mStartRequested = false;
        }
    }

    public ScaleType getScaleType() {
        return scaleType;
    }

    /**
     * Default is FIT_CROP
     *
     * @param scaleType
     */
    public void setScaleType(ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        int previewWidth = 320;
        int previewHeight = 240;
        if (mCameraSource != null) {
            Size size = mCameraSource.getPreviewSize();
            if (size != null) {
                previewWidth = size.getWidth();
                previewHeight = size.getHeight();
            }
        }

        // Swap width and height sizes when in portrait, since it will be rotated 90 degrees
        if (isPortraitMode()) {
            int tmp = previewWidth;
            //noinspection SuspiciousNameCombination
            previewWidth = previewHeight;
            previewHeight = tmp;
        }

        final int layoutWidth = right - left;
        final int layoutHeight = bottom - top;

        // Computes height and width for potentially doing fit width.
        if (scaleType == ScaleType.CENTER) {
            layoutChildCenter(previewWidth, previewHeight, layoutWidth, layoutHeight);
        } else {
            layoutChildFitCrop(previewWidth, previewHeight, layoutWidth, layoutHeight);
        }

        try {
            startIfReady();
        } catch (SecurityException se) {
            Timber.e("Do not have permission to start the camera");
        } catch (IOException e) {
            Timber.e("Could not start camera source.");
        }
    }

    /**
     * Calculate child (Surface view) layout size
     */
    private void layoutChildCenter(int previewWidth, int previewHeight, int layoutWidth, int layoutHeight) {
        int childWidth = layoutWidth;
        int childHeight = (int) (((float) layoutWidth / (float) previewWidth) * previewHeight);

        // If height is too tall using fit width, does fit height instead.
        if (childHeight > layoutHeight) {
            childHeight = layoutHeight;
            childWidth = (int) (((float) layoutHeight / (float) previewHeight) * previewWidth);
        }

        for (int i = 0; i < getChildCount(); ++i) {
            getChildAt(i).layout(0, 0, childWidth, childHeight);
        }
    }

    /**
     * Calculate child (Surface view) layout size
     */
    private void layoutChildFitCrop(int previewWidth, int previewHeight, int layoutWidth, int layoutHeight) {
        int childWidth = 0;
        int childHeight = 0;

        double previewRatio = (double) previewWidth / previewHeight;
        double layoutRatio = (double) layoutWidth / layoutHeight;

        // Calculate by width
        if (layoutRatio > previewRatio) {
            childWidth = layoutWidth;
            childHeight = (int) (childWidth / previewRatio);
        }
        // Calculate by height
        else {
            childHeight = layoutHeight;
            childWidth = (int) (childHeight * previewRatio);
        }

        for (int i = 0; i < getChildCount(); ++i) {
            getChildAt(i).layout(0, 0, childWidth, childHeight);
        }
    }

    private boolean isPortraitMode() {
        int orientation = mContext.getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            return false;
        }
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            return true;
        }

        Timber.d("isPortraitMode returning false by default");
        return false;
    }

    public enum ScaleType {FIT_CROP, CENTER}

    private class SurfaceCallback implements SurfaceHolder.Callback {
        @Override
        public void surfaceCreated(SurfaceHolder surface) {
            mSurfaceAvailable = true;
            try {
                startIfReady();
            } catch (SecurityException se) {
                Timber.e("Do not have permission to start the camera");
            } catch (IOException e) {
                Timber.e("Could not start camera source.");
            }
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder surface) {
            mSurfaceAvailable = false;
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        }
    }
}
