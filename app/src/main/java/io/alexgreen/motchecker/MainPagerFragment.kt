package io.alexgreen.motchecker

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton
import io.alexgreen.motchecker.views.SwipeDirection
import kotlinx.android.synthetic.main.main_pager_fragment.*

/**
 * Fragment hosting a ViewPager and bottom nav bar allowing navigation between the main top-level destinations in the
 * application.
 */
class MainPagerFragment : Fragment(), AppBarHost, FabHost, NavHost {

    companion object {
        fun newInstance() = MainPagerFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.main_pager_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        pagerToolbar.setNavigationIcon(R.drawable.ic_menu_grey_24dp)
        setupBottomNavigation()
        setupPager()
    }

    override val appBar: AppBarLayout?
        get() = pagerAppBar

    override val primaryFab: ExtendedFloatingActionButton?
        get() = fabPrimary

    override val secondaryFab: ExtendedFloatingActionButton?
        get() = fabSecondary

    override fun navigateToSearch() {
        bottomNav.selectedItemId = R.id.bottomNavSearch
    }

    private fun setupBottomNavigation() {
        bottomNav.setOnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.bottomNavSearch -> mainViewPager.currentItem = SEARCH_POSITION
                R.id.bottomNavVehicles -> mainViewPager.currentItem = VEHICLES_POSITION
                R.id.bottomNavReminders -> mainViewPager.currentItem = REMINDERS_POSITION
            }
            true
        }
    }

    private fun setupPager() {
        mainViewPager.apply {
            offscreenPageLimit = 2
            allowedSwipeDirection = SwipeDirection.NONE
            adapter = MainPagerAdapter(childFragmentManager)
        }
    }
}

/**
 * Defines a controller which hosts an [AppBarLayout].
 */
interface AppBarHost {
    /**
     * The [AppBarLayout] being hosted.
     */
    val appBar: AppBarLayout?
}

/**
 * Defines a controller which hosts [ExtendedFloatingActionButton]s.
 */
interface FabHost {
    /**
     * The primary [ExtendedFloatingActionButton] being hosted.
     */
    val primaryFab: ExtendedFloatingActionButton?
    /**
     * The secondary [ExtendedFloatingActionButton] being hosted.
     */
    val secondaryFab: ExtendedFloatingActionButton?
}

/**
 * Defines a controller which handles navigation.
 */
interface NavHost {
    /**
     * Navigates to search.
     */
    fun navigateToSearch()
}
