package io.alexgreen.motchecker

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import io.alexgreen.motchecker.search.SearchFragment
import io.alexgreen.motchecker.settings.SettingsFragment
import io.alexgreen.motchecker.vehicles.saved.SavedVehiclesFragment

const val SEARCH_POSITION = 0
const val VEHICLES_POSITION = 1
const val REMINDERS_POSITION = 2

/**
 * Pager adapter used to display the main top-level fragments.
 */
class MainPagerAdapter(
    fragmentManager: FragmentManager
) : FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getCount() = 3

    override fun getItem(position: Int) = when (position) {
        SEARCH_POSITION -> SearchFragment.newInstance()
        VEHICLES_POSITION -> SavedVehiclesFragment.newInstance()
        // TODO: Change to reminders fragment
        REMINDERS_POSITION -> SettingsFragment.newInstance()
        else -> throw IllegalAccessException()
    }
}
