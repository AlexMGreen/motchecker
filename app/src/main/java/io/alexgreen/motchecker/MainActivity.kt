package io.alexgreen.motchecker

import android.os.Bundle
import androidx.fragment.app.commit
import dagger.android.support.DaggerAppCompatActivity

/**
 * Main application entry point. Used to host fragments in the [R.id.fragmentContainer].
 */
class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.commit {
                replace(R.id.fragmentContainer, MainPagerFragment.newInstance())
            }
        }
    }
}
