package io.alexgreen.motchecker.db

import androidx.room.Database
import androidx.room.RoomDatabase
import io.alexgreen.motchecker.vehicles.Vehicle

@Database(entities = [Vehicle::class], version = 1)
abstract class VehicleDatabase : RoomDatabase() {
    abstract fun vehicleDao(): VehicleDao
}
