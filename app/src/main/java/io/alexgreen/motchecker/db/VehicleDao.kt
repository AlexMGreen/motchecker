package io.alexgreen.motchecker.db

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import io.alexgreen.motchecker.vehicles.Vehicle

@Dao
interface VehicleDao {

    @Query("SELECT * FROM vehicles WHERE userSavedManually ORDER BY itemOrder")
    fun getAllManuallySavedVehicles(): LiveData<List<Vehicle>>

    @Query("SELECT * FROM vehicles ORDER BY lastSearchedDate DESC LIMIT :limit")
    fun getRecentVehicles(limit: Int): LiveData<List<Vehicle>>

    @Query("SELECT * FROM vehicles ORDER BY lastSearchedDate DESC")
    fun getAllRecentVehicles(): LiveData<List<Vehicle>>

    @Query("SELECT * FROM vehicles WHERE registration = :registrationNumber LIMIT 1")
    suspend fun getVehicle(registrationNumber: String): Vehicle?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertVehicle(vehicle: Vehicle)

    @Update
    suspend fun updateVehicle(vehicle: Vehicle)
}
