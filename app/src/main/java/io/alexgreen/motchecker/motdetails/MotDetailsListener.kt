package io.alexgreen.motchecker.motdetails

/**
 * A listener to be notified of events regarding MOT details.
 */
interface MotDetailsListener {
    /**
     * Invoked upon successful retrieval of MOT details for a vehicle.
     */
    fun onMotDetailsRetrieved()

    /**
     * Invoked upon the user closing the view of the MOT details.
     */
    fun onMotDetailsClosed()
}
