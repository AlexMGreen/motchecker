package io.alexgreen.motchecker.motdetails.vehicledetails

import io.alexgreen.motchecker.extensions.formatDayMonthYear
import io.alexgreen.motchecker.mot.MotHistory
import io.alexgreen.motchecker.mot.parsedFirstUsedDate
import io.alexgreen.motchecker.motdetails.MotDetailsItem

/**
 * ViewModel used to display a summary of the given [MotHistory] by the MotDetailsAdapter.
 */
class VehicleDetailsViewModel(val motHistory: MotHistory) : MotDetailsItem {
    /**
     * The make of the vehicle detailed in the given [motHistory].
     */
    val make = motHistory.make
    /**
     * The model of the vehicle detailed in the given [motHistory], with the first letter of each work capitalized.
     */
    val model = motHistory.model.toLowerCase().split(' ').joinToString(separator = " ") { it.capitalize() }
    /**
     * The primary colour of the vehicle detailed in the given [motHistory].
     */
    val primaryColour = motHistory.primaryColour
    /**
     * The fuel type of the vehicle detailed in the given [motHistory].
     */
    val fuelType = motHistory.fuelType
    /**
     * The first used date of the vehicle detailed in the given [motHistory], returned of the form "dd MMM yyyy"
     * (e.g. 13 Nov 2010).
     */
    val firstUsedDate: String by lazy { motHistory.parsedFirstUsedDate.formatDayMonthYear() }
}
