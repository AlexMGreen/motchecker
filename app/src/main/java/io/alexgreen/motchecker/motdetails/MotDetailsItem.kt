package io.alexgreen.motchecker.motdetails

/**
 * Common interface for all items to be displayed within the MOT details list, to faciliate with differing item view
 * types within [MotDetailsAdapter].
 */
interface MotDetailsItem
