package io.alexgreen.motchecker.motdetails.status

import io.alexgreen.motchecker.mot.MotHistory
import io.alexgreen.motchecker.mot.parsedExpiryDate
import io.alexgreen.motchecker.motdetails.MotDetailsItem
import org.threeten.bp.Clock
import org.threeten.bp.LocalDate
import javax.inject.Inject

/**
 * ViewModel used to display a summary of the given [MotHistory] by the MotDetailsAdapter.
 */
class MotStatusViewModel @Inject constructor(
    private val motHistory: MotHistory,
    private val clock: Clock
) : MotDetailsItem {

    /**
     * Whether or not the vehicle detailed in [motHistory] currently has a valid MOT.
     */
    // TODO: Check if any of the tests are more recent than the most recent expiry date and see if they failed, e.g.
    // If there is an expiry date passed the current date, but most recent test was a fail then MOT not valid
    val hasValidMot = motHistory.motTests
        .mapNotNull { it.parsedExpiryDate }
        .any { parsedExpiryDate ->
            parsedExpiryDate.isEqual(LocalDate.now(clock)) || parsedExpiryDate.isAfter(LocalDate.now(clock))
        }

    /**
     * The most recent MOT expiry date from the [motHistory]'s list of past [MotHistory.motTests]. As this property
     * is not present in [MotHistory.motTests]s which were not passed by the vehicle, this date can be considered to
     * be the most recent date to which the vehicle has a valid MOT.
     */
    val latestExpiryDate = motHistory.motTests.sortedByDescending { it.parsedExpiryDate }.first().parsedExpiryDate
}
