package io.alexgreen.motchecker.motdetails.mileage

import io.alexgreen.motchecker.mot.MotHistory
import io.alexgreen.motchecker.mot.formattedMileage
import io.alexgreen.motchecker.motdetails.MotDetailsItem

/**
 * ViewModel to aid with displaying a mileage title within the mot details by the MotDetailsAdapter.
 */
class MileageTitleViewModel(motHistory: MotHistory) : MotDetailsItem {

    /***
     * The total number of miles recorded for the vehicle, based on the test within [MotHistory.motTests] which has the
     * largest odometerValue. The odometerValue is parsed to an int and then formatted to a string with commas, e.g.
     * "86,318".
     */
    val totalMiles: String? by lazy {
        // TODO: Check odometerUnit and convert to miles if needed
        motHistory.motTests.maxBy { it.odometerValue.toInt() }?.formattedMileage
    }
}
