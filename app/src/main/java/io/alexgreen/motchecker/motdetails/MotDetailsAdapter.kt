package io.alexgreen.motchecker.motdetails

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.motdetails.history.MotHistoryTitleViewHolder
import io.alexgreen.motchecker.motdetails.history.MotHistoryTitleViewModel
import io.alexgreen.motchecker.motdetails.history.MotTestViewHolder
import io.alexgreen.motchecker.motdetails.history.MotTestViewModel
import io.alexgreen.motchecker.motdetails.mileage.MileageTitleViewHolder
import io.alexgreen.motchecker.motdetails.mileage.MileageTitleViewModel
import io.alexgreen.motchecker.motdetails.mileage.MileageViewHolder
import io.alexgreen.motchecker.motdetails.mileage.MileageViewModel
import io.alexgreen.motchecker.motdetails.status.MotStatusViewHolder
import io.alexgreen.motchecker.motdetails.status.MotStatusViewModel
import io.alexgreen.motchecker.motdetails.vehicledetails.VehicleDetailsViewHolder
import io.alexgreen.motchecker.motdetails.vehicledetails.VehicleDetailsViewModel

private const val TYPE_VEHICLE_DETAILS = 0
private const val TYPE_STATUS = 1
private const val TYPE_MILEAGE_TITLE = 3
private const val TYPE_MILEAGE = 4
private const val TYPE_HISTORY_TITLE = 5
private const val TYPE_MOT_TEST = 6

/**
 * An adapter used to display a list of [MotDetailsItem], comprising the overall MOT details for a vehicle.
 */
class MotDetailsAdapter(
    private val motDetailsItems: List<MotDetailsItem>,
    private val onItemClicked: (MotDetailsItem, MotDetailsAdapter) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    /**
     * The position of the MOT History title item within the list of MOT details.
     */
    val motHistoryTitlePos: Int
        get() = motDetailsItems.indexOfFirst { it is MotHistoryTitleViewModel }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            TYPE_VEHICLE_DETAILS ->
                VehicleDetailsViewHolder(inflater.inflate(R.layout.item_vehicle_details, parent, false))
            TYPE_STATUS -> MotStatusViewHolder(inflater.inflate(R.layout.item_mot_status, parent, false)).apply {
                itemView.setOnClickListener { onItemClicked(motDetailsItems[adapterPosition], this@MotDetailsAdapter) }
            }
            TYPE_MILEAGE_TITLE -> MileageTitleViewHolder(inflater.inflate(R.layout.item_mileage_title, parent, false))
            TYPE_MILEAGE -> MileageViewHolder(inflater.inflate(R.layout.item_mot_mileage, parent, false))
            TYPE_HISTORY_TITLE ->
                MotHistoryTitleViewHolder(inflater.inflate(R.layout.item_mot_history_title, parent, false))
            TYPE_MOT_TEST -> MotTestViewHolder(inflater.inflate(R.layout.item_mot_test, parent, false))
            else -> throw UnsupportedOperationException()
        }
    }

    override fun getItemCount() = motDetailsItems.size

    override fun getItemViewType(position: Int) = when (motDetailsItems[position]) {
        is VehicleDetailsViewModel -> TYPE_VEHICLE_DETAILS
        is MotStatusViewModel -> TYPE_STATUS
        is MileageTitleViewModel -> TYPE_MILEAGE_TITLE
        is MileageViewModel -> TYPE_MILEAGE
        is MotHistoryTitleViewModel -> TYPE_HISTORY_TITLE
        is MotTestViewModel -> TYPE_MOT_TEST
        else -> 0
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is VehicleDetailsViewHolder -> holder.bind(motDetailsItems[position])
            is MotStatusViewHolder -> holder.bind(motDetailsItems[position])
            is MileageTitleViewHolder -> holder.bind(motDetailsItems[position])
            is MileageViewHolder -> holder.bind(motDetailsItems[position])
            is MotHistoryTitleViewHolder -> holder.bind(motDetailsItems[position])
            is MotTestViewHolder -> holder.bind(motDetailsItems[position])
        }
    }
}
