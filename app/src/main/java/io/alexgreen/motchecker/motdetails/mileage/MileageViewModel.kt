package io.alexgreen.motchecker.motdetails.mileage

import com.github.mikephil.charting.data.Entry
import io.alexgreen.motchecker.mot.MotHistory
import io.alexgreen.motchecker.mot.parsedCompletedDate
import io.alexgreen.motchecker.mot.parsedFirstUsedDate
import io.alexgreen.motchecker.motdetails.MotDetailsItem

/**
 * ViewModel used to display mileage history of the vehicle detailed in the the given [MotHistory], by the
 * MotDetailsAdapter.
 */
class MileageViewModel(motHistory: MotHistory) : MotDetailsItem {

    /**
     * A list of [Entry] to be displayed within a mileage history chart.
     */
    val mileageEntries by lazy {
        listOf(Entry(motHistory.parsedFirstUsedDate.toEpochDay().toFloat(), 0f)) + motHistory.motTests
            .distinctBy { it.parsedCompletedDate }
            .sortedBy { it.parsedCompletedDate }
            .map { Entry(it.parsedCompletedDate.toEpochDay().toFloat(), it.odometerValue.toFloat()) }
    }
}
