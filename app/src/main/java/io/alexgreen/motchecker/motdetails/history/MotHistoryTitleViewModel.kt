package io.alexgreen.motchecker.motdetails.history

import androidx.annotation.StringRes
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.mot.MotHistory
import io.alexgreen.motchecker.mot.didPass
import io.alexgreen.motchecker.motdetails.MotDetailsItem

/**
 * ViewModel to aid with displaying a MOT history title within the mot details by the MotDetailsAdapter.
 */
class MotHistoryTitleViewModel(motHistory: MotHistory) : MotDetailsItem {

    /**
     * The [StringRes] of the title to be displayed.
     */
    @StringRes
    val title = R.string.mot_history

    /**
     * The number, as an [Int], of previously passed MOT tests in the given motHistory.
     */
    val numberOfTestsPassed = motHistory.motTests.count { it.didPass }
    /**
     * The number, as an [Int], of previously failed MOT tests in the given motHistory.
     */
    val numberOfTestsFailed = motHistory.motTests.count { !it.didPass }
}
