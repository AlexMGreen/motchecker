package io.alexgreen.motchecker.motdetails

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.os.postDelayed
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import androidx.transition.ChangeBounds
import androidx.transition.Transition
import dagger.android.support.DaggerFragment
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.extensions.elevateTo
import io.alexgreen.motchecker.extensions.extraNotNull
import io.alexgreen.motchecker.extensions.fadeIn
import io.alexgreen.motchecker.extensions.fadeOut
import io.alexgreen.motchecker.extensions.mainHandler
import io.alexgreen.motchecker.extensions.slideIn
import io.alexgreen.motchecker.extensions.slideOut
import io.alexgreen.motchecker.extensions.toPx
import io.alexgreen.motchecker.mot.MotHistory
import io.alexgreen.motchecker.mot.Result
import io.alexgreen.motchecker.motdetails.status.MotStatusViewModel
import kotlinx.android.synthetic.main.mot_details_fragment.*
import timber.log.Timber
import javax.inject.Inject

private const val REGISTRATION_NUMBER_KEY = "registrationNumber"

/**
 * Fragment used to display to the user details of a vehicle's MOT history. Uses the [MotDetailsViewModel] to retrieve
 * a list of [MotDetailsItem] to be displayed using the [MotDetailsAdapter].
 */
class MotDetailsFragment : DaggerFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    private val viewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(MotDetailsViewModel::class.java)
    }
    private val registrationNumber by extraNotNull<String>(REGISTRATION_NUMBER_KEY)
    private val changeBoundsTransition: Transition
        get() = ChangeBounds().setInterpolator(DecelerateInterpolator())
    private var isSearchViewElevated = false
    private var motDetailsListener: MotDetailsListener? = null

    companion object {
        /**
         * Creates a new fragment to display [MotHistory] details for the vehicle with the given [registrationNumber].
         */
        fun newInstance(registrationNumber: String) = MotDetailsFragment().apply {
            val normalizedReg = registrationNumber.replace("\\s|\\n".toRegex(), "")
            arguments = bundleOf(REGISTRATION_NUMBER_KEY to normalizedReg)
            sharedElementEnterTransition = changeBoundsTransition
            enterTransition = changeBoundsTransition
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        motDetailsListener = parentFragment as? MotDetailsListener
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? = inflater.inflate(R.layout.mot_details_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.init(registrationNumber)
        saveFab.slideOut()
        with(numberPlateSearch) {
            setText(registrationNumber)
            isEditable = false
            setIconClickListener { activity?.supportFragmentManager?.popBackStack() }
        }
        observeMotHistory()
        elevateSearchViewOnScroll()
        numberPlateSearch.setIconClickListener {
            motDetailsListener?.onMotDetailsClosed()
        }
    }

    private fun setupFab() {
        if (viewModel.isVehicleManuallySaved.value == true) {
            return
        }
        saveFab.slideIn()
        saveFab.setOnClickListener {
            viewModel.updateVehicleAsManuallySaved()
        }
        observeSavedVehicle()
    }

    private fun setupMotDetailsList(motDetailsItems: List<MotDetailsItem>) {
        val smoothScroller = object : LinearSmoothScroller(context) {
            override fun getVerticalSnapPreference() = SNAP_TO_START
        }
        motDetailsList.apply {
            adapter = MotDetailsAdapter(motDetailsItems) { clickedItem, adapter ->
                when (clickedItem) {
                    is MotStatusViewModel -> {
                        smoothScroller.targetPosition = adapter.motHistoryTitlePos
                        motDetailsList.layoutManager?.startSmoothScroll(smoothScroller)
                    }
                }
            }
            layoutManager = LinearLayoutManager(requireContext())
            scheduleLayoutAnimation()
            invalidate()
        }
    }

    private fun observeMotHistory() {
        viewModel.motHistory.observe(viewLifecycleOwner, Observer { motResult ->
            setLoading(motResult)
            when (motResult) {
                is Result.Success -> {
                    Timber.d("Successfully retrieved MOT history: $motResult")
                    setupMotDetailsList(viewModel.getMotDetailsItems(motResult.data))
                    setupFab()
                    motDetailsListener?.onMotDetailsRetrieved()
                }
                // TODO: Handle [Result.Error] by showing message
                is Result.Error -> Timber.w("Failed to retrieve MOT history: ${motResult.message}")
            }
        })
    }

    private fun observeSavedVehicle() {
        viewModel.isVehicleManuallySaved.observe(viewLifecycleOwner, Observer { isSaved ->
            if (isSaved && saveFab.isVisible) {
                saveFab.icon = ContextCompat.getDrawable(requireContext(), R.drawable.ic_check_white_24dp)
                saveFab.shrink()
                mainHandler.postDelayed(500) { saveFab?.slideOut() }
            }
        })
    }

    private fun setLoading(motResult: Result<MotHistory>) = when (motResult) {
        is Result.Loading -> progress.fadeIn(startDelayMillis = 250)
        else -> progress.fadeOut()
    }

    private fun elevateSearchViewOnScroll() {
        motDetailsList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (!motDetailsList.canScrollVertically(-1)) {
                    // At top of the list
                    saveFab.extend()
                    if (isSearchViewElevated) {
                        numberPlateSearchCard.elevateTo(4.toPx().toFloat())
                        isSearchViewElevated = false
                    }
                } else {
                    saveFab.shrink()
                    if (!isSearchViewElevated) {
                        numberPlateSearchCard.elevateTo(24.toPx().toFloat())
                        isSearchViewElevated = true
                    }
                }
            }
        })
    }
}
