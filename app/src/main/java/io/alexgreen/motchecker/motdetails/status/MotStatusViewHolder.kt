package io.alexgreen.motchecker.motdetails.status

import android.os.Handler
import android.view.View
import androidx.core.os.postDelayed
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.extensions.formatDayMonthYear
import io.alexgreen.motchecker.motdetails.MotDetailsItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_mot_status.*

/**
 * ViewHolder used to display an [MotStatusViewModel].
 */
class MotStatusViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    override val containerView: View?
        get() = itemView

    init {
        Handler().postDelayed(delayInMillis = 500) { motValidAnimation?.playAnimation() }
    }

    /**
     * Binds the given [motDetailsItem] to the [itemView]. If [motDetailsItem] is not of type [MotStatusViewModel]
     * then no action will be performed.
     */
    fun bind(motDetailsItem: MotDetailsItem) {
        if (motDetailsItem !is MotStatusViewModel) {
            return
        }

        motValidAnimation.setAnimation(
            if (motDetailsItem.hasValidMot) R.raw.check_mark_success else R.raw.unapproved_cross
        )
        motValidText.text = HtmlCompat.fromHtml(
            itemView.context.getString(
                if (motDetailsItem.hasValidMot) R.string.mot_valid_text else R.string.mot_expired_text,
                motDetailsItem.latestExpiryDate?.formatDayMonthYear()
            ),
            HtmlCompat.FROM_HTML_MODE_LEGACY
        )
    }
}
