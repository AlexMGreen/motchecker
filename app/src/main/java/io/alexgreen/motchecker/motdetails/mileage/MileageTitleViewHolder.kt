package io.alexgreen.motchecker.motdetails.mileage

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.motdetails.MotDetailsItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_mileage_title.*

/**
 * ViewHolder used to display a [MileageTitleViewModel].
 */
class MileageTitleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    override val containerView: View?
        get() = itemView

    /**
     * Binds the given [motDetailsItem] to the [itemView]. If [motDetailsItem] is not of type [MileageTitleViewModel]
     * then no action will be performed.
     */
    fun bind(motDetailsItem: MotDetailsItem) {
        if (motDetailsItem !is MileageTitleViewModel) {
            return
        }
        mileageText.text = itemView.context.getString(R.string.mot_miles_minimum, motDetailsItem.totalMiles)
    }
}
