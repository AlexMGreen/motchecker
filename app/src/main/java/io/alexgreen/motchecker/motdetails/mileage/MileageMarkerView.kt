package io.alexgreen.motchecker.motdetails.mileage

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.widget.TextView
import androidx.annotation.LayoutRes
import com.github.mikephil.charting.charts.Chart
import com.github.mikephil.charting.components.MarkerView
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.highlight.Highlight
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.extensions.formatDayMonthYear
import kotlinx.android.synthetic.main.marker_mileage.view.*
import org.threeten.bp.LocalDate
import java.text.DecimalFormat

/**
 * A marker to be displayed within a mileage chart, displaying individual mileage values when chart entry clicked.
 */
@SuppressLint("ViewConstructor")
class MileageMarkerView(context: Context, @LayoutRes layoutRes: Int, chart: Chart<*>) : MarkerView(context, layoutRes) {
    private val mileageText: TextView = findViewById(R.id.mileageText)
    private val mileageFormatter = DecimalFormat("#,###,###")

    init {
        chartView = chart
    }

    override fun refreshContent(entry: Entry?, highlight: Highlight?) {
        entry?.x?.toLong()?.let { longDate ->
            mileageDate.text = LocalDate.ofEpochDay(longDate).formatDayMonthYear()
        }
        mileageText.text = context.getString(
            R.string.mileage_chart_label_format, mileageFormatter.format(entry?.y?.toInt())
        )
        super.refreshContent(entry, highlight)
    }

    override fun draw(canvas: Canvas?, posX: Float, posY: Float) {
        super.draw(canvas, posX, posY)
        getOffsetForDrawingAtPoint(posX, posY)
    }
}
