package io.alexgreen.motchecker.motdetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import io.alexgreen.motchecker.mot.MotHistory
import io.alexgreen.motchecker.mot.MotRepository
import io.alexgreen.motchecker.mot.Result
import io.alexgreen.motchecker.motdetails.history.MotHistoryTitleViewModel
import io.alexgreen.motchecker.motdetails.history.MotTestViewModel
import io.alexgreen.motchecker.motdetails.mileage.MileageTitleViewModel
import io.alexgreen.motchecker.motdetails.mileage.MileageViewModel
import io.alexgreen.motchecker.motdetails.status.MotStatusViewModel
import io.alexgreen.motchecker.motdetails.vehicledetails.VehicleDetailsViewModel
import io.alexgreen.motchecker.vehicles.Vehicle
import io.alexgreen.motchecker.vehicles.VehicleRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.threeten.bp.Clock
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import javax.inject.Inject

/**
 * ViewModel used to facilitate displaying to the user details of a vehicle's MOT history and storing vehicles.
 */
class MotDetailsViewModel @Inject constructor(
    private val clock: Clock,
    private val motRepository: MotRepository,
    private val vehicleRepository: VehicleRepository
) : ViewModel() {

    /**
     * A LiveData result which emits the result of an [MotHistory] information request for the vehicle with the
     * registrationNumber of which the ViewModel was [init] with.  While the request is ongoing, the [Result] will be
     * of type [Result.Loading]. If successful, the [Result] will be of type [Result.Success], with
     * [Result.Success.data] containing the [MotHistory]. Otherwise, [Result] will be of type [Result.Error] with a
     * [Result.Error.message].
     */
    val motHistory: LiveData<Result<MotHistory>>
        get() = _motHistory
    private val _motHistory = MutableLiveData<Result<MotHistory>>()

    private var automaticallySavedVehicle: Vehicle? = null

    /**
     * Whether or not the vehicle has previously been manually saved, to determine whether or not to offer the user the
     * option to do so.
     */
    val isVehicleManuallySaved: LiveData<Boolean>
        get() = _isVehicleManuallySaved
    private val _isVehicleManuallySaved = MutableLiveData<Boolean>()

    /**
     * Initialises the ViewModel with the given [registrationNumber]. MOT details for the vehicle with the given
     * [registrationNumber] will be retrieved if not already done so.
     */
    fun init(registrationNumber: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _isVehicleManuallySaved.postValue(vehicleRepository.isVehicleManuallySaved(registrationNumber))
        }
        if (_motHistory.value == null) {
            getMotHistory(registrationNumber)
        }
    }

    private fun getMotHistory(registrationNumber: String) {
        viewModelScope.launch(Dispatchers.IO) {
            _motHistory.postValue(Result.Loading)
            val historyResult = motRepository.getMotHistory(registrationNumber)
            _motHistory.postValue(historyResult)
            if (historyResult is Result.Success) {
                val existingRecord = vehicleRepository.getVehicle(historyResult.data.registration)
                if (existingRecord == null) {
                    saveVehicle(historyResult.data)
                } else {
                    updateLastSearchedDate(existingRecord)
                }
            }
        }
    }

    private fun saveVehicle(motHistory: MotHistory) = viewModelScope.launch(Dispatchers.IO) {
        val vehicle = motHistory.toVehicle()
        vehicleRepository.saveVehicle(vehicle)
        automaticallySavedVehicle = vehicle
    }

    private fun updateLastSearchedDate(existingRecord: Vehicle) = viewModelScope.launch(Dispatchers.IO) {
        val updated = existingRecord.copy(lastSearchedDate = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC))
        vehicleRepository.updateVehicle(updated)
        automaticallySavedVehicle = updated
    }

    fun updateVehicleAsManuallySaved() = viewModelScope.launch(Dispatchers.IO) {
        automaticallySavedVehicle?.let {
            vehicleRepository.updateVehicle(it.copy(userSavedManually = true))
            _isVehicleManuallySaved.postValue(true)
        }
    }

    /**
     * Returns a list of [MotDetailsItem] for the given [motHistory]. If [motHistory] is null then an empty list will
     * be returned.
     */
    fun getMotDetailsItems(motHistory: MotHistory?): List<MotDetailsItem> {
        if (motHistory == null) return emptyList()
        return listOf(
            VehicleDetailsViewModel(motHistory),
            MotStatusViewModel(motHistory, clock),
            MileageTitleViewModel(motHistory),
            MileageViewModel(motHistory),
            MotHistoryTitleViewModel(motHistory)
        ).plus(motHistory.motTests.map { MotTestViewModel(it) }.sortedByDescending { it.completedDate })
    }

    private fun MotHistory.toVehicle() = Vehicle(
        registration,
        make,
        model,
        firstUsedDate,
        fuelType,
        primaryColour
    )
}
