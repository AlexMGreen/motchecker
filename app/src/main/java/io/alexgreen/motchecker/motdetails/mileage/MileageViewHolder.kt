package io.alexgreen.motchecker.motdetails.mileage

import android.view.View
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.components.AxisBase
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.extensions.formatMonthYear
import io.alexgreen.motchecker.motdetails.MotDetailsItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_mot_mileage.*
import org.threeten.bp.LocalDate

/**
 * ViewHolder used to to display an [MileageViewModel].
 */
class MileageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    override val containerView: View?
        get() = itemView

    init {
        with(mileageChart) {
            xAxis.position = XAxis.XAxisPosition.BOTTOM
            xAxis.granularity = 1f
            axisRight.isEnabled = false
            axisLeft.axisMinimum = 0f
            description.isEnabled = false
            legend.isEnabled = false
            animateY(1000, Easing.EaseOutBounce)
            setScaleEnabled(false)
            invalidate()
            extraRightOffset = 18f
            marker = MileageMarkerView(context, R.layout.marker_mileage, this)
        }
    }

    /**
     * Binds the given [motDetailsItem] to the [itemView]. If [motDetailsItem] is not of type [MileageViewModel]
     * then no action will be performed.
     */
    fun bind(motDetailsItem: MotDetailsItem) {
        if (motDetailsItem is MileageViewModel) {
            val dataSet = setupDataSet(motDetailsItem)
            with(mileageChart) {
                xAxis.valueFormatter = object : ValueFormatter() {
                    override fun getAxisLabel(value: Float, axis: AxisBase?): String {
                        return LocalDate.ofEpochDay(value.toLong()).formatMonthYear()
                    }
                }
                xAxis.setLabelCount(6, true)
                data = LineData(dataSet)
            }
        }
    }

    private fun setupDataSet(motDetailsItem: MileageViewModel) = LineDataSet(motDetailsItem.mileageEntries, "").apply {
        val colorPrimary = ContextCompat.getColor(itemView.context, R.color.colorPrimary)
        mode = LineDataSet.Mode.LINEAR
        color = ContextCompat.getColor(itemView.context, R.color.colorPrimary)
        circleColors = listOf(colorPrimary)
        valueTextSize = 8f
        setDrawValues(false)
        setDrawFilled(true)
        fillColor = colorPrimary
        fillAlpha = 20
    }
}
