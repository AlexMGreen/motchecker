package io.alexgreen.motchecker.motdetails.history

import android.os.Handler
import android.view.View
import androidx.core.os.postDelayed
import androidx.recyclerview.widget.RecyclerView
import io.alexgreen.motchecker.extensions.fadeIn
import io.alexgreen.motchecker.motdetails.MotDetailsItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_mot_history_title.*

/**
 * ViewHolder used to display a [MotHistoryTitleViewModel].
 */
class MotHistoryTitleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    override val containerView: View?
        get() = itemView

    init {
        Handler().postDelayed(delayInMillis = 500) {
            passedCount?.fadeIn(500)
            failedCount?.fadeIn(500)
            passedAnimation?.playAnimation()
            failedAnimation?.playAnimation()
        }
    }

    /**
     * Binds the given [motDetailsItem] to the [itemView]. If [motDetailsItem] is not of type [MotHistoryTitleViewModel]
     * then no action will be performed.
     */
    fun bind(motDetailsItem: MotDetailsItem) {
        if (motDetailsItem !is MotHistoryTitleViewModel) {
            return
        }
        motHistoryTitle.text = itemView.context.getString(motDetailsItem.title)
        passedCount.text = motDetailsItem.numberOfTestsPassed.toString()
        failedCount.text = motDetailsItem.numberOfTestsFailed.toString()
    }
}
