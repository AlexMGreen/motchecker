package io.alexgreen.motchecker.motdetails.history

import io.alexgreen.motchecker.extensions.formatDayMonthYear
import io.alexgreen.motchecker.mot.MotTest
import io.alexgreen.motchecker.mot.didPass
import io.alexgreen.motchecker.mot.formattedMileage
import io.alexgreen.motchecker.mot.parsedCompletedDate
import io.alexgreen.motchecker.mot.parsedExpiryDate
import io.alexgreen.motchecker.motdetails.MotDetailsItem

/**
 * ViewModel to aid with displaying the results of a single [MotTest] within the MOT history for a vehicle.
 */
class MotTestViewModel(motTest: MotTest) : MotDetailsItem {
    /**
     * Whether or not the [MotTest] resulted in a pass.
     */
    val didPass = motTest.didPass
    /**
     * The date on which the [MotTest] was completed.
     */
    val completedDate = motTest.parsedCompletedDate
    /**
     * The mileage of the vehicle at the time of the [completedDate].
     */
    // TODO: Check odometerUnit and convert to miles if needed
    val testMileage = motTest.formattedMileage
    /**
     * The test number of the given [MotTest].
     */
    val testNumber = motTest.motTestNumber
    /**
     * A list of reasons for failure identified during the [MotTest].
     */
    // TODO: Distinguish PRS ("Pass after Rectification at Station") and dangerous failures from regular failure/major
    val reasonsForFailure = motTest.rfrAndComments.filter {
        it.type == "PRS" || it.type == "FAIL" || it.type == "DANGEROUS"
    }
    /**
     * A list of 'minor' defects identified during the [MotTest].
     */
    val minorDefects = motTest.rfrAndComments.filter { it.type == "MINOR" }
    /**
     * A list of advisories identified during the [MotTest].
     */
    val advisories = motTest.rfrAndComments.filter { it.type == "ADVISORY" }
    /**
     * The expiry date of the [MotTest]. Will be null if [MotTest.didPass] is not true.
     */
    val expiryDate = motTest.parsedExpiryDate?.formatDayMonthYear()
}
