package io.alexgreen.motchecker.motdetails.vehicledetails

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import io.alexgreen.motchecker.motdetails.MotDetailsItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_vehicle_details.*

/**
 * ViewHolder used to display an [VehicleDetailsViewModel].
 */
class VehicleDetailsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    override val containerView: View?
        get() = itemView

    /**
     * Binds the given [motDetailsItem] to the [itemView]. If [motDetailsItem] is not of type [VehicleDetailsViewModel]
     * then no action will be performed.
     */
    fun bind(motDetailsItem: MotDetailsItem) {
        if (motDetailsItem is VehicleDetailsViewModel) {
            summaryMake.text = motDetailsItem.make
            summaryModel.text = motDetailsItem.model
            summaryColorText.text = motDetailsItem.primaryColour
            summaryFirstUsedDate.text = motDetailsItem.firstUsedDate
        }
    }
}
