package io.alexgreen.motchecker.motdetails.history

import android.text.SpannableStringBuilder
import android.text.style.BulletSpan
import android.view.View
import androidx.core.content.ContextCompat
import androidx.core.text.inSpans
import androidx.recyclerview.widget.RecyclerView
import io.alexgreen.motchecker.R
import io.alexgreen.motchecker.extensions.formatDayMonthYear
import io.alexgreen.motchecker.extensions.visibleIf
import io.alexgreen.motchecker.mot.RfrAndComment
import io.alexgreen.motchecker.motdetails.MotDetailsItem
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_mot_test.*

/**
 * ViewHolder used to display a [MotTestViewModel].
 */
class MotTestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), LayoutContainer {
    override val containerView: View?
        get() = itemView

    /**
     * Binds the given [motDetailsItem] to the [itemView]. If [motDetailsItem] is not of type [MotTestViewModel]
     * then no action will be performed.
     */
    fun bind(motDetailsItem: MotDetailsItem) {
        if (motDetailsItem !is MotTestViewModel) {
            return
        }

        testDateText.text = motDetailsItem.completedDate.formatDayMonthYear()
        setTestResult(motDetailsItem)
        testResultMileage.text = itemView.context.getString(R.string.mot_test_miles, motDetailsItem.testMileage)
        testNumber.text = motDetailsItem.testNumber
        showExpiryDate(motDetailsItem)
        showReasonsForFailure(motDetailsItem)
        showMinorDefects(motDetailsItem)
        showAdvisories(motDetailsItem)
    }

    private fun setTestResult(motTestViewModel: MotTestViewModel) {
        testResultText.text = itemView.context.getString(
            if (motTestViewModel.didPass) R.string.mot_test_passed else R.string.mot_test_failed
        )
        testResultText.setTextColor(
            ContextCompat.getColor(
                itemView.context, if (motTestViewModel.didPass) R.color.successGreen else R.color.failureRed
            )
        )
    }

    private fun showExpiryDate(motTestViewModel: MotTestViewModel) {
        val hasExpiryDate = !motTestViewModel.expiryDate.isNullOrBlank()
        testResultExpiryDateTitle.visibleIf { hasExpiryDate }
        testResultExpiryDate.visibleIf { hasExpiryDate }
        testResultExpiryDate.text = motTestViewModel.expiryDate
    }

    private fun showReasonsForFailure(motDetailsItem: MotTestViewModel) {
        val hasReasonsForFailure = motDetailsItem.reasonsForFailure.isNotEmpty()
        testResultReasonsForFailureTitle.visibleIf { hasReasonsForFailure }
        testResultReasonsForFailure.visibleIf { hasReasonsForFailure }
        testResultReasonsForFailure.text = motDetailsItem.reasonsForFailure.transformToDefectsString()
    }

    private fun showMinorDefects(motTestViewModel: MotTestViewModel) {
        val hasMinorDefects = motTestViewModel.minorDefects.isNotEmpty()
        testResultMinorDefectsTitle.visibleIf { hasMinorDefects }
        testResultMinorDefects.visibleIf { hasMinorDefects }
        testResultMinorDefects.text = motTestViewModel.minorDefects.transformToDefectsString()
    }

    private fun showAdvisories(motTestViewModel: MotTestViewModel) {
        val hasAdvisories = motTestViewModel.advisories.isNotEmpty()
        testResultAdvisoriesTitle.visibleIf { hasAdvisories }
        testResultAdvisories.visibleIf { hasAdvisories }
        testResultAdvisories.text = motTestViewModel.advisories.transformToDefectsString()
    }

    private fun List<RfrAndComment>.transformToDefectsString() = SpannableStringBuilder().apply {
        this@transformToDefectsString.map { inSpans(BulletSpan(16)) { appendln(it.text) } }
    }.removeSuffix("\n")
}
